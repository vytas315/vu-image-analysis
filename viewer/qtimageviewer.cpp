#include <QBarCategoryAxis>
#include <QBarSeries>
#include <QBarSet>
#include <QFileDialog>
#include <QImageReader>
#include <QInputDialog>
#include <QPalette>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QValueAxis>
#include <iostream>

#include "qtimageviewer.hpp"
#include <image.hpp>

QtImageViewer::QtImageViewer(QWidget *parent) :
  QMainWindow(parent){
  init();
};

void  QtImageViewer::init(){
  _lImageLabel = new QLabel;
  _lImageLabel->setBackgroundRole(QPalette::Base);
  _lImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  _lImageLabel->setScaledContents(false);

  _lScrollArea = new QScrollArea;
  _lScrollArea->setBackgroundRole(QPalette::Dark);
  _lScrollArea->setWidget(_lImageLabel);
  _lScrollArea->setVisible(true);

  _rImageLabel = new QLabel;
  _rImageLabel->setBackgroundRole(QPalette::Base);
  _rImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  _rImageLabel->setScaledContents(false);

  _rScrollArea = new QScrollArea;
  _rScrollArea->setBackgroundRole(QPalette::Dark);
  _rScrollArea->setWidget(_rImageLabel);
  _rScrollArea->setVisible(true);

  _mainSplitter = new QSplitter();
  _leftSplitter = new QSplitter(Qt::Vertical);
  _rightSplitter = new QSplitter(Qt::Vertical);
  _mainSplitter->addWidget(_leftSplitter);
  _mainSplitter->addWidget(_rightSplitter);
  _leftSplitter->addWidget(_lScrollArea);
  _rightSplitter->addWidget(_rScrollArea);

  _lChartView = new QtCharts::QChartView();
  _leftSplitter->addWidget(_lChartView);

  _rChartView = new QtCharts::QChartView();
  _rightSplitter->addWidget(_rChartView);

  setCentralWidget(_mainSplitter);
  createActions();
}

QtImageViewer::~QtImageViewer(){
  delete(_lImageLabel);
  delete(_lScrollArea);
  delete(_rImageLabel);
  delete(_rScrollArea);
  delete(_fileMenu);
  delete(_fileOpenAction);
  delete(_quitAction);
};

void QtImageViewer::createActions(){
  _fileOpenAction = new QAction(tr("&Open..."), this);
  _fileOpenAction->setShortcut(QKeySequence::Open);
  connect(_fileOpenAction, SIGNAL(triggered()), this, SLOT(openFile()));

  _quitAction = new QAction(tr("&Quit..."), this);
  _quitAction->setShortcut(QKeySequence::Quit);
  connect(_quitAction, SIGNAL(triggered()), this, SLOT(quit()));

  _combineAction = new QAction(tr("&Combine Files..."), this);
  connect(_combineAction, SIGNAL(triggered()), this, SLOT(combineImages()));

  _transformAction = new QAction(tr("&Affine Transform..."), this);
  connect(_transformAction, SIGNAL(triggered()), this, SLOT(transformImages()));

  _intensityTransformAction = new QAction(tr("&Intensity Transform..."), this);
  connect(_intensityTransformAction, SIGNAL(triggered()), this, SLOT(intensityTransformImages()));

  _filterImageAction = new QAction(tr("&Filter..."), this);
  connect(_filterImageAction, SIGNAL(triggered()), this, SLOT(filterImages()));

  _fourierTransformAction = new QAction(tr("&Fourier Transform..."), this);
  connect(_fourierTransformAction, SIGNAL(triggered()), this, SLOT(fourierTransformImages()));

  _createImageAction = new QAction(tr("&Generate Fourier Image..."), this);
  connect(_createImageAction, SIGNAL(triggered()), this, SLOT(generatedFourier()));

  _fourierFilterAction = new QAction(tr("&Fourier Filters..."), this);
  connect(_fourierFilterAction, SIGNAL(triggered()), this, SLOT(filterFourier()));

  _countSignalsAction = new QAction(tr("&Count Signals..."), this);
  connect(_countSignalsAction, SIGNAL(triggered()), this, SLOT(countSignals()));

  _fileMenu = menuBar()->addMenu(tr("&File"));
  _fileMenu->addAction(_fileOpenAction);
  _fileMenu->addAction(_combineAction);
  _fileMenu->addAction(_transformAction);
  _fileMenu->addAction(_intensityTransformAction);
  _fileMenu->addAction(_filterImageAction);
  _fileMenu->addAction(_fourierTransformAction);
  _fileMenu->addAction(_createImageAction);
  _fileMenu->addAction(_fourierFilterAction);
  _fileMenu->addAction(_countSignalsAction);
  _fileMenu->addAction(_quitAction);
}

void QtImageViewer::quit(){
	close();
};

void QtImageViewer::openFile(){
  QFileDialog dialog(this, tr("Open File"));
  QString filename = dialog.getOpenFileName(this, "Select image to open");
  std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
  showFile(filename);
}

void QtImageViewer::combineImages(){
  QFileDialog dialog1(this, tr("Open File"));
  QString filename1 = dialog1.getOpenFileName(this, "Select first image to combine");
  QFileDialog dialog2(this, tr("Open File"));
  QString filename2 = dialog2.getOpenFileName(this, "Select second image to combine");
  QFileDialog dialog3(this, tr("Open File"));
  QString filename3 = dialog3.getOpenFileName(this, "Select third image to combine");
  Image* myImage = new Image(filename1.toStdString(), filename2.toStdString(), filename3.toStdString());
  std::cout<<"Combined image created..."<<std::endl;
  showLeftImage(myImage);
}

void QtImageViewer::transformImages(){
  QFileDialog dialog(this, tr("Open File"));
  QString filename = dialog.getOpenFileName(this, "Select image to affine transform");
  std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
  Image* myImage = new Image(filename.toStdString());
  Image* transformedImage = new Image(filename.toStdString());
  transformedImage->transformImage();
  showLeftImage(myImage);
  showRightImage(transformedImage);
}

void QtImageViewer::intensityTransformImages(){
  QFileDialog dialog(this, tr("Open File"));
  QString filename = dialog.getOpenFileName(this, "Select image to intensity transform");
  std::cout<<"Opening: "<<filename.toStdString()<<std::endl;

  QStringList items;
  bool ok;
  items << tr("Power Law") << tr("Piece Wise Linear") << tr("Histogram Normalize");
  QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                        tr("Choose transform:"), items, 0, false, &ok);
  if (ok && !item.isEmpty()){
    Image* myImage = new Image(filename.toStdString());
    Image* transformedImage = new Image(myImage);
    showLeftImage(myImage);
    if(item == "Power Law")
      transformedImage->applyPowerTransform();
    else if(item == "Piece Wise Linear")
      transformedImage->applyPieceWiseLinearTransform();
    else if(item == "Histogram Normalize")
      transformedImage->applyHistogramNormalization();
    showRightImage(transformedImage);
  }
}

void QtImageViewer::filterImages(){
  QFileDialog dialog(this, tr("Open File"));
  QString filename = dialog.getOpenFileName(this, "Select image to filter");
  std::cout<<"Opening: "<<filename.toStdString()<<std::endl;

  QStringList items;
  bool ok;
  items <<
    tr("Laplacian (b)") <<
    tr("Sharpen by original a + b (c)") <<
    tr("Sobel Gradient (d)") <<
    tr("Sobel Smoothed (e)") <<
    tr("Mask c x e (f)") <<
    tr("Sharpen by a + f (g)") <<
    tr("Power Law on g (h)") <<
    tr("Blur");
  QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                        tr("Choose filter:"), items, 0, false, &ok);
  if (ok && !item.isEmpty()){
    Image* myImage = new Image(filename.toStdString());
    std::cout<<"Creating float image..."<<std::endl;
    if(item == "Laplacian (b)"){
      ImageFloat filteredImage(myImage);
      filteredImage.applyLaplacian(myImage);
      std::cout<<"Normalizing image..."<<std::endl;
      Image* normalizedImage = filteredImage.getNormalizedTo8Bits();
      std::cout<<"Rendering normalized image..."<<std::endl;
      showRightImage(normalizedImage);
    }
    else if(item == "Sharpen by original a + b (c)"){
      ImageFloat laplacImage(myImage);
      laplacImage.applyLaplacian(myImage);
      ImageFloat sharpenedImage(myImage);
      sharpenedImage.add(laplacImage.getNormalizedTo8Bits());
      Image* normalizedImage = sharpenedImage.getNormalizedTo8Bits();
      std::cout<<"Rendering normalized image..."<<std::endl;
      showRightImage(normalizedImage);
    }
    else if(item == "Sobel Gradient (d)"){
      ImageFloat sobelImage(myImage);
      sobelImage.applySobel(myImage);
      Image* normalizedImage = sobelImage.getNormalizedTo8Bits();
      showRightImage(normalizedImage);
    }
    else if(item == "Sobel Smoothed (e)"){
      ImageFloat sobelImage(myImage);
      sobelImage.applySobel(myImage);
      Image* normalizedSobel = sobelImage.getNormalizedTo8Bits();
      ImageFloat smoothedSobelImage(normalizedSobel);
      smoothedSobelImage.apply3x3Smoothing(normalizedSobel);
      Image* normalizedImage = smoothedSobelImage.getNormalizedTo8Bits();
      showRightImage(normalizedImage);
      // showLeftImage(normalizedSobel);
    }
    else if(item == "Mask c x e (f)"){
      // Recreating (c)
      ImageFloat laplacImage(myImage);
      laplacImage.applyLaplacian(myImage);
      ImageFloat sharpenedImage(myImage);
      sharpenedImage.add(laplacImage.getNormalizedTo8Bits());
      Image* cImage = sharpenedImage.getNormalizedTo8Bits();

      // Recreating (e)
      ImageFloat sobelImage(myImage);
      sobelImage.applySobel(myImage);
      Image* normalizedSobel = sobelImage.getNormalizedTo8Bits();
      ImageFloat smoothedSobelImage(normalizedSobel);
      smoothedSobelImage.apply3x3Smoothing(normalizedSobel);
      Image* eImage = smoothedSobelImage.getNormalizedTo8Bits();
      
      ImageFloat multipliedImage(cImage);
      multipliedImage.multiply(eImage);
      Image* normalizedImage = multipliedImage.getNormalizedTo8Bits();
      showRightImage(normalizedImage);
    }
    else if(item == "Sharpen by a + f (g)"){
      // Recreating (c)
      ImageFloat laplacImage(myImage);
      laplacImage.applyLaplacian(myImage);
      ImageFloat sharpenedImage(myImage);
      sharpenedImage.add(laplacImage.getNormalizedTo8Bits());
      Image* cImage = sharpenedImage.getNormalizedTo8Bits();

      // Recreating (e)
      ImageFloat sobelImage(myImage);
      sobelImage.applySobel(myImage);
      Image* normalizedSobel = sobelImage.getNormalizedTo8Bits();
      ImageFloat smoothedSobelImage(normalizedSobel);
      smoothedSobelImage.apply3x3Smoothing(normalizedSobel);
      Image* eImage = smoothedSobelImage.getNormalizedTo8Bits();
      
      // Recreating (f)
      ImageFloat multipliedImage(cImage);
      multipliedImage.multiply(eImage);
      Image* myfImage = multipliedImage.getNormalizedTo8Bits();

      ImageFloat sharpendAtoFimage(myImage);
      sharpendAtoFimage.add(myfImage);

      Image* normalizedImage = sharpendAtoFimage.getNormalizedTo8Bits();
      showRightImage(normalizedImage);
    }
    else if(item == "Power Law on g (h)"){
      // Recreating (c)
      ImageFloat laplacImage(myImage);
      laplacImage.applyLaplacian(myImage);
      ImageFloat sharpenedImage(myImage);
      sharpenedImage.add(laplacImage.getNormalizedTo8Bits());
      Image* cImage = sharpenedImage.getNormalizedTo8Bits();

      // Recreating (e)
      ImageFloat sobelImage(myImage);
      sobelImage.applySobel(myImage);
      Image* normalizedSobel = sobelImage.getNormalizedTo8Bits();
      ImageFloat smoothedSobelImage(normalizedSobel);
      smoothedSobelImage.apply3x3Smoothing(normalizedSobel);
      Image* eImage = smoothedSobelImage.getNormalizedTo8Bits();
      
      // Recreating (f)
      ImageFloat multipliedImage(cImage);
      multipliedImage.multiply(eImage);
      Image* myfImage = multipliedImage.getNormalizedTo8Bits();

      ImageFloat sharpendAtoFimage(myImage);
      sharpendAtoFimage.add(myfImage);

      Image* normalizedImage = sharpendAtoFimage.getNormalizedTo8Bits();
      normalizedImage->applyPowerTransform();
      showRightImage(normalizedImage);
    }
    else if(item == "Blur") {
      ImageFloat blurImage(myImage);
      blurImage.apply3x3Smoothing(myImage);
      Image* normalizedImage = blurImage.getNormalizedTo8Bits();
      showRightImage(normalizedImage);
    }
    showLeftImage(myImage);
  }
}

void QtImageViewer::fourierTransformImages(){
  QFileDialog dialog(this, tr("Open File"));
  QString filename = dialog.getOpenFileName(this, "Select image to transform");
  std::cout<<"Opening: "<<filename.toStdString()<<std::endl;

  QStringList items;
  bool ok;
  items <<
    tr("Pad image (b)") <<
    tr("b shifted for periodicity (c)") <<
    tr("DFT (d)") <<
    tr("IDFT (e)") <<
    tr("e Cropped (f)");

  QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                        tr("Choose transform step:"), items, 0, false, &ok);
  if (ok && !item.isEmpty()){
    Image* myImage = new Image(filename.toStdString());
    Image* transformedImage = new Image(myImage);
    std::cout<<"Creating float image..."<<std::endl;
    if(item == "Pad image (b)"){
      std::cout<<"Padding image..."<<std::endl;
      fourierStepB(transformedImage);
    }
    else if(item == "b shifted for periodicity (c)"){
      std::cout<<"Shifting image..."<<std::endl;
      fourierStepC(transformedImage);
    }
    else if(item == "DFT (d)"){
      std::cout<<"DFT image..."<<std::endl;
      transformedImage = fourierStepD(transformedImage);
      std::cout<<"Rechecking pixel width again: "<<transformedImage->getWidth()<<std::endl;
    }
    else if(item == "IDFT (e)"){
      std::cout<<"IDFT image..."<<std::endl;
      transformedImage = fourierStepE(transformedImage);
    }
    else if(item == "e Cropped (f)"){
      std::cout<<"Cropping image..."<<std::endl;
      transformedImage = fourierStepF(transformedImage);
    }
    else {
      std::cout<<"No menu item matched..."<<std::endl;
    }
    // QFileDialog dialogSave(this, tr("Save File"));
    // QString saveFilename = dialogSave.getSaveFileName(this, "Save to:");
    // std::cout<<"Saving: "<<saveFilename.toStdString()<<std::endl;
    // transformedImage->saveOutput(saveFilename.toStdString());
    showLeftImage(myImage);
    std::cout<<"Rechecking pixel width again 2: "<<transformedImage->getWidth()<<std::endl;
    showRightImage(transformedImage);
  }
}

void QtImageViewer::generatedFourier(){
  QStringList items;
  bool ok;
  items <<
    tr("Image 1") <<
    tr("Image 2") <<
    tr("Image 3") <<
    tr("Image 4") <<
    tr("Image 5");

  QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                        tr("Choose image to generate:"), items, 0, false, &ok);

  QMessageBox saveDialog;
  saveDialog.setText("Do you want to save the output image?");
  // saveDialog.setInformativeText("Do you want to save output?");
  saveDialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
  saveDialog.setDefaultButton(QMessageBox::Discard);
  int saveFileDialogResult = saveDialog.exec();
  if (ok && !item.isEmpty()){
    Image* myImage = nullptr;
    Image* transformedImage = nullptr;
    std::cout<<"Generating images..."<<std::endl;
    if(item == "Image 1"){
      std::cout<<"Generating image 1..."<<std::endl;
      ImageFloat generatedImageA;
      generatedImageA.generateImageA();
      myImage = generatedImageA.getNormalizedTo8Bits();
      generatedImageA.fourierShift();
      generatedImageA.fourierTransform();
      transformedImage = generatedImageA.getNormalizedTo8Bits();
    }
    else if(item == "Image 2"){
      std::cout<<"Generating image 2..."<<std::endl;
      ImageFloat generatedImage;
      generatedImage.generateImageB();
      myImage = generatedImage.getNormalizedTo8Bits();
      generatedImage.fourierShift();
      generatedImage.fourierTransform();
      transformedImage = generatedImage.getNormalizedTo8Bits();
    }
    else if(item == "Image 3"){
      std::cout<<"Generating image 3..."<<std::endl;
      ImageFloat generatedImage;
      generatedImage.generateImageC();
      myImage = generatedImage.getNormalizedTo8Bits();
      generatedImage.fourierShift();
      generatedImage.fourierTransform();
      transformedImage = generatedImage.getNormalizedTo8Bits();
    }
    else if(item == "Image 4"){
      std::cout<<"Generating image 4..."<<std::endl;
      ImageFloat generatedImage;
      generatedImage.generateImageD();
      myImage = generatedImage.getNormalizedTo8Bits();
      generatedImage.fourierShift();
      generatedImage.fourierTransform();
      transformedImage = generatedImage.getNormalizedTo8Bits();
    }
        else if(item == "Image 5"){
      std::cout<<"Generating image 5..."<<std::endl;
      ImageFloat generatedImage;
      generatedImage.generateImageE();
      myImage = generatedImage.getNormalizedTo8Bits();
      generatedImage.fourierShift();
      generatedImage.fourierTransform();
      transformedImage = generatedImage.getNormalizedTo8Bits();
    }
    else {
      std::cout<<"No menu item matched..."<<std::endl;
    }

    if (saveFileDialogResult == QMessageBox::Save){
      saveImage(transformedImage);
    }
    showLeftImage(myImage);
    showRightImage(transformedImage);
  }
}

void QtImageViewer::saveImage(Image* image) {
    QFileDialog dialogSave(this, tr("Save File"));
    QString saveFilename = dialogSave.getSaveFileName(this, "Save to:");
    std::cout<<"Saving: "<<saveFilename.toStdString()<<std::endl;
    image->saveOutput(saveFilename.toStdString());
}

bool QtImageViewer::fourierStepB(Image* image) {
  image->fourierPad();
  return true;
}

bool QtImageViewer::fourierStepC(Image* image) {
  fourierStepB(image);
  ImageFloat shiftImageFloat(image);
  shiftImageFloat.fourierShift();
  delete(image);
  image = shiftImageFloat.getTruncatedTo8Bits();
  return true;
}
Image* QtImageViewer::fourierStepD(Image* image) {
  image->fourierPad();
  ImageFloat shiftImageFloat(image);
  shiftImageFloat.fourierShift();
  shiftImageFloat.fourierTransform();
  Image* currentImage = shiftImageFloat.getNormalizedTo8Bits();
  return currentImage;
}

Image* QtImageViewer::fourierStepE(Image* image) {
  image->fourierPad();
  ImageFloat shiftImageFloat(image);
  shiftImageFloat.fourierShift();
  shiftImageFloat.inverseFourierTransform();
  shiftImageFloat.fourierShift();
  return shiftImageFloat.getTruncatedTo8Bits();
}

Image* QtImageViewer::fourierStepF(Image* image) {
  Image* currImage = fourierStepE(image);
  currImage->fourierCrop();
  return currImage;
}

void QtImageViewer::filterFourier(){
  QStringList items;
  bool ok;
  items <<
    tr("Low Pass Ideal Filter") <<
    tr("High Pass Ideal Filter") <<
    tr("Low Pass Butterworth Filter") <<
    tr("High Pass Butterworth Filter") <<
    tr("Low Pass Gaussian Filter") <<
    tr("High Pass Gaussian Filter");

  QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                        tr("Choose a filter:"), items, 0, false, &ok);

  QMessageBox saveDialog;
  saveDialog.setText("Do you want to save the output image?");
  saveDialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
  saveDialog.setDefaultButton(QMessageBox::Discard);
  int saveFileDialogResult = saveDialog.exec();

  QFileDialog dialog(this, tr("Open File"));
  QString filename = dialog.getOpenFileName(this, "Select image to transform");
  std::cout<<"Opening: "<<filename.toStdString()<<std::endl;

  bool ok_radius;
  double radius = QInputDialog::getDouble(this, tr("Filter Radius"),
    tr("Amount:"), 160, 0, 1000, 2, &ok_radius,
    Qt::WindowFlags(), 1);
  if (!ok_radius) {
    return;
  }

  if (ok && !item.isEmpty()){
    Image* myImage = new Image(filename.toStdString());
    Image* transformedImage = nullptr;
    std::cout<<"Filtering images..."<<std::endl;

    myImage->fourierPad();
    ImageFloat fourierImage(myImage);
    ImageFloat fourierFilter(myImage);
    fourierImage.fourierShift();

    if(item == "Low Pass Ideal Filter"){
      fourierImage.applyLowPassIdealFilter(radius);
      fourierFilter.renderLowPassIdealFilter(radius);
    }
    else if(item == "High Pass Ideal Filter"){
      fourierImage.applyHighPassIdealFilter(radius);
      fourierFilter.renderHighPassIdealFilter(radius);
    }
    if(item == "Low Pass Butterworth Filter"){
      fourierImage.applyLowPassButterworthFilter(radius, 2);
      fourierFilter.renderLowPassButterworthFilter(radius, 2);
    }
    else if(item == "High Pass Butterworth Filter"){
      fourierImage.applyHighPassButterworthFilter(radius, 2);
      fourierFilter.renderHighPassButterworthFilter(radius, 2);
    }
    if(item == "Low Pass Gaussian Filter"){
      fourierImage.applyLowPassGaussianFilter(radius);
      fourierFilter.renderLowPassGaussianFilter(radius);
    }
    else if(item == "High Pass Gaussian Filter"){
      fourierImage.applyHighPassGaussianFilter(radius);
      fourierFilter.renderHighPassGaussianFilter(radius);
    }
    else {
      std::cout<<"No menu item matched..."<<std::endl;
    }

    fourierImage.fourierShift();
    transformedImage = fourierImage.getTruncatedTo8Bits();
    transformedImage->fourierCrop();

    delete(myImage);
    myImage = fourierFilter.getTruncatedTo8Bits();

    if (saveFileDialogResult == QMessageBox::Save){
      saveImage(transformedImage);
      saveImage(myImage);
    }
    showLeftImage(myImage);
    showRightImage(transformedImage);
  }
}

void QtImageViewer::showFile(const QString filename){  

  Image* myImage = new Image(filename.toStdString());
  showLeftImage(myImage);
}

void QtImageViewer::showLeftImage(Image* myImage) {

  QImage::Format format = QImage::Format_Invalid;
  if(myImage->getChannels() == 3) {
    format = QImage::Format_RGB888;
  }
  else if (myImage->getChannels() == 1)
    format = QImage::Format_Grayscale8;

  std::cout<<"Rendering image..."<<std::endl;
  std::cout<<"Width: "<<myImage->getWidth()<<std::endl;
  std::cout<<"Height: "<<myImage->getHeight()<<std::endl;

  unsigned long hist[256] = {};
  myImage->getHistogram(hist);

  QtCharts::QBarSet *bset = new QtCharts::QBarSet("Intensities");
  unsigned int maxInt{0};
  for(unsigned int i : hist) { (*bset) << i; maxInt = std::max(maxInt,i);  }
  
  QtCharts::QBarSeries *bseries = new QtCharts::QBarSeries();
  bseries->append(bset);

  QtCharts::QChart *chart = new QtCharts::QChart();
  chart->addSeries(bseries);
  chart->setTitle("Intensity Histogram");
  chart->setAnimationOptions(QtCharts::QChart::NoAnimation);
  
  QtCharts::QValueAxis *xAxis = new  QtCharts::QValueAxis();
  xAxis->setRange(0,255);
  
  QtCharts::QValueAxis *yAxis = new QtCharts::QValueAxis();
  yAxis->setRange(0,maxInt);

  // Seems you must add axis to chart first, then to bseries
  chart->addAxis(xAxis, Qt::AlignBottom);
  chart->addAxis(yAxis, Qt::AlignLeft);

  bseries->attachAxis(xAxis);
  bseries->attachAxis(yAxis);
  bset->setBorderColor(QColor("black"));

  chart->legend()->setVisible(true);
  chart->legend()->setAlignment(Qt::AlignBottom);
  
  _lChartView->setChart(chart);
  _lChartView->setRenderHint(QPainter::Antialiasing);

  QImage qImg(myImage->getImageData(),
	      myImage->getWidth(),
	      myImage->getHeight(),
        myImage->getWidth()*myImage->getChannels()*myImage->getBPC()/8,
        format);
  std::cout<<"QT image created..."<<std::endl;
  _lImageLabel->setPixmap(QPixmap::fromImage(qImg));
  _lImageLabel->resize(_lImageLabel->pixmap()->size());
  _lScrollArea->setVisible(true);

  update(); /// Force Qt to redraw
  delete(myImage); /// Data is copied to Qt, we can delete our image.
}

void QtImageViewer::showRightImage(Image *img) {

  QImage::Format format = QImage::Format_Invalid;

  if(img->getChannels() == 3){
    format = QImage::Format_RGB888;
  }
  else if (img->getChannels() == 1)
    format = QImage::Format_Grayscale8;

  std::cout<<"Rendering second image..."<<std::endl;
  std::cout<<"Width: "<<img->getWidth()<<std::endl;
  std::cout<<"Height: "<<img->getHeight()<<std::endl;

  unsigned long hist[256] = {};
  img->getHistogram(hist);

  QtCharts::QBarSet *bset = new QtCharts::QBarSet("Intensities");
  unsigned int maxInt{0};
  for(unsigned int i : hist) { (*bset) << i; maxInt = std::max(maxInt,i);  }
  
  QtCharts::QBarSeries *bseries = new QtCharts::QBarSeries();
  bseries->append(bset);
  QtCharts::QChart *chart = new QtCharts::QChart();
  chart->addSeries(bseries);
  chart->setTitle("Intensity Histogram");
  chart->setAnimationOptions(QtCharts::QChart::NoAnimation);
  
  QtCharts::QValueAxis *xAxis = new  QtCharts::QValueAxis();
  xAxis->setRange(0,255);
  
  QtCharts::QValueAxis *yAxis = new QtCharts::QValueAxis();
  yAxis->setRange(0,maxInt);

  // Seems you must add axis to chart first, then to bseries
  chart->addAxis(xAxis, Qt::AlignBottom);
  chart->addAxis(yAxis, Qt::AlignLeft);

  bseries->attachAxis(xAxis);
  bseries->attachAxis(yAxis);
  bset->setBorderColor(QColor("black"));

  chart->legend()->setVisible(true);
  chart->legend()->setAlignment(Qt::AlignBottom);
  
  _rChartView->setChart(chart);
  _lChartView->setRenderHint(QPainter::Antialiasing);

  // Copy image data to Qt
  QImage qImg(img->getImageData(),
	      img->getWidth(),
	      img->getHeight(),
	      img->getWidth()*img->getChannels()*img->getBPC()/8,
	      format);
  
  // Tell Qt to show this image data
  _rImageLabel->setPixmap(QPixmap::fromImage(qImg));
  _rImageLabel->resize(_rImageLabel->pixmap()->size());
  _rScrollArea->setVisible(true);
  delete(img);
  update(); // For Qt to redraw with new image
}
