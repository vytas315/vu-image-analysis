TEMPLATE = app
DEPENDPATH += ./
INCLUDEPATH += ./ ../ ../imagelib ## path to imagelib
RESOURCES += 

CONFIG += qt c++11 debug_and_release
QT += widgets gui charts

TARGET = imageviewer 
LIBS+= -L../imagelib/ -limagelib -ljpeg -ltiff -lfftw3 -lm


HEADERS += qtimageviewer.hpp

SOURCES += imageviewer.cpp \
	qtimageviewer.cpp \
	assignmentThreeViewer.cpp

## add call to compile image library
imagelib.target = myimagelibtarget
imagelib.commands = make -w -C ../imagelib/;
imagelib.depends =
QMAKE_EXTRA_TARGETS += imagelib
PRE_TARGETDEPS += myimagelibtarget