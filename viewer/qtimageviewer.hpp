#ifndef QT_IMAGE_VIEWER_MAINWINDOW_HPP
#define QT_IMAGE_VIEWER_MAINWINDOW_HPP

#include <QChartView>
#include <QMainWindow>
#include <QLabel>
#include <QImage>
#include <QScrollArea>
#include <QSplitter>
#include <QWidget>

#include <image.hpp>

class QtImageViewer : public QMainWindow{
  Q_OBJECT /// Qt Syntax. Must be included.
public:
  QtImageViewer(QWidget *parent=nullptr);
  QtImageViewer(const QString filename, QWidget *parent=nullptr);
  virtual ~QtImageViewer();


public slots: /// Qt Syntax Signal Slot mechanism.
  void showFile(const QString filename);
  void showLeftImage(Image* myImage);
  void showRightImage(Image* myImage);

  void openFile();
  void quit();
  void combineImages();
  void transformImages();
  void intensityTransformImages();
  void filterImages();
  void fourierTransformImages();
  void generatedFourier();
  void saveImage(Image* image);
  void filterFourier();
  void countSignals();

private:
  void init();
  void createActions();
  void createMenus();
  // Qt Image related
  QImage _lImage;
  QLabel *_lImageLabel{nullptr};
  QScrollArea *_lScrollArea{nullptr};

  QImage _rImage;
  QLabel *_rImageLabel{nullptr};
  QScrollArea *_rScrollArea{nullptr};

  // Qt interface related
  QMenu *_fileMenu{nullptr};
  QAction *_fileOpenAction{nullptr};
  QAction *_quitAction{nullptr};
  QAction *_combineAction{nullptr};
  QAction *_transformAction{nullptr};
  QAction *_intensityTransformAction{nullptr};
  QAction *_filterImageAction{nullptr};
  QAction *_fourierTransformAction{nullptr};
  QAction *_createImageAction{nullptr};
  QAction *_fourierFilterAction{nullptr};
  QAction *_countSignalsAction{nullptr};

  QSplitter *_mainSplitter{nullptr};
  QSplitter *_leftSplitter{nullptr};
  QSplitter *_rightSplitter{nullptr};
  QtCharts::QChartView *_lChartView{nullptr};
  QtCharts::QChartView *_rChartView{nullptr};

  bool fourierStepB(Image* image);
  bool fourierStepC(Image* image);
  Image* fourierStepD(Image* image);
  Image* fourierStepE(Image* image);
  Image* fourierStepF(Image* image);
};
#endif
