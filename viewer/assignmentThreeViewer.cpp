#include <QBarCategoryAxis>
#include <QBarSeries>
#include <QBarSet>
#include <QFileDialog>
#include <QImageReader>
#include <QInputDialog>
#include <QPalette>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QValueAxis>
#include <iostream>

#include "qtimageviewer.hpp"
#include <image.hpp>
#include <math.h>

void QtImageViewer::countSignals(){
  QStringList items;
  bool ok;
  items <<
    tr("Count objects DAPI") <<
    tr("Count objects Acridine") <<
    tr("Count objects FITC") <<
    tr("Full counts") <<
    tr("Find islands") <<
    tr("Find bad islands") <<
    tr("Find bad holes") <<
    tr("Find breaks") <<
    tr("Check bottles");

  QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                        tr("Choose a filter:"), items, 0, false, &ok);
  if(!ok || item.isEmpty()) {
    return;
  }

  QMessageBox saveDialog;
  saveDialog.setText("Do you want to save the output image?");
  saveDialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
  saveDialog.setDefaultButton(QMessageBox::Discard);
  int saveFileDialogResult = saveDialog.exec();
  Image* myImage = nullptr;
  Image* transformedImage = nullptr;

  if(item == "Count objects DAPI"){
    std::cout<<"Count objects dapi..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);
    transformedImage -> applyThreshold(30);
    transformedImage -> errodeSymmetricSquare(7);
    transformedImage -> dilateSymmetricSquare(7);
    transformedImage -> fillHoles(150);
    transformedImage -> countConnected();
  }
  else if(item == "Count objects Acridine"){
    std::cout<<"Count objects acridine..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);
    transformedImage -> applyThreshold(150);
    transformedImage -> countConnected();
  }
  else if(item == "Count objects FITC"){
    std::cout<<"Count objects acridine..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);
    transformedImage -> applyThreshold(50);
    transformedImage -> dilateSymmetricSquare(3);
    transformedImage -> errodeSymmetricSquare(3);
    transformedImage -> countConnected();
  }
  else if(item == "Full counts"){
    std::cout<<"Counting all types..."<<std::endl;

    QFileDialog acridineDialog(this, tr("Open Acridine File"));
    QString acridineFilename = acridineDialog.getOpenFileName(this, "Select Acridine image");
    Image* acridineImage = new Image(acridineFilename.toStdString());

    QFileDialog DAPIDialog(this, tr("Open DAPI File"));
    QString DAPIFilename = DAPIDialog.getOpenFileName(this, "Select DAPI image");
    Image* DAPIImage = new Image(DAPIFilename.toStdString());

    QFileDialog FITCDialog(this, tr("Open FITC File"));
    QString FITCFilename = FITCDialog.getOpenFileName(this, "Select FITC image");
    Image* FITCImage = new Image(FITCFilename.toStdString());

    DAPIImage -> applyThreshold(30);
    DAPIImage -> errodeSymmetricSquare(7);
    DAPIImage -> dilateSymmetricSquare(7);
    DAPIImage -> fillHoles(150);
    long* DAPILabelMap = DAPIImage -> getLabelMap(255);
    std::set<int> DAPILabels = DAPIImage -> getUniqueLabels(255);

    acridineImage -> applyThreshold(150);
    std::set<int> acridineLabels = acridineImage -> getUniqueLabels(255);

    FITCImage -> applyThreshold(50);
    FITCImage -> dilateSymmetricSquare(3);
    FITCImage -> errodeSymmetricSquare(3);
    std::set<int> FITCLabels = FITCImage -> getUniqueLabels(255);

    std::map<long,long> acridineResults;
    std::map<long,long> acridineHoleResults;
    std::map<long,long> FITCResults;
    std::map<long,long> FITCHoleResults;

    for (auto const& acridineLabel : acridineLabels) {
      if(acridineImage -> getPixel(acridineLabel) == 255) {
        if (DAPIImage -> getPixel(acridineLabel) == 255) {
          // Acridine corresponds to DAPI cell
          long dapiLabel = DAPILabelMap[acridineLabel];
          if(acridineResults.count(dapiLabel) == 0) {
            acridineResults[dapiLabel] = 1;
          }
          else
          {
            acridineResults[dapiLabel] = acridineResults[dapiLabel] + 1;
          }
        }
        else {
          // Acridine is in a hole
          if(acridineHoleResults.count(acridineLabel) == 0) {
            acridineHoleResults[acridineLabel] = 1;
          }
          else
          {
            acridineHoleResults[acridineLabel] = acridineHoleResults[acridineLabel] + 1;
          }
        }
      }
    }

    for (auto const& FITCLabel : FITCLabels) {
      if(FITCImage -> getPixel(FITCLabel) == 255) {
        if (DAPIImage -> getPixel(FITCLabel) == 255) {
          // Acridine corresponds to DAPI cell
          long dapiLabel = DAPILabelMap[FITCLabel];
          if(FITCResults.count(dapiLabel) == 0) {
            FITCResults[dapiLabel] = 1;
          }
          else
          {
            FITCResults[dapiLabel] = FITCResults[dapiLabel] + 1;
          }
        }
        else {
          // Acridine is in a hole
          if(FITCHoleResults.count(FITCLabel) == 0) {
            FITCHoleResults[FITCLabel] = 1;
          }
          else
          {
            FITCHoleResults[FITCLabel] = FITCHoleResults[FITCLabel] + 1;
          }
        }
      }
    }

    for (auto const& DAPILabel : DAPILabels) {
      int yIndex = floor(DAPILabel / DAPIImage -> getWidth());
      int xIndex = DAPILabel - yIndex * DAPIImage -> getWidth();
      std::cout << DAPILabel  // string (key)
                  << ',' << xIndex
                  << ',' << yIndex
                  << ',' << acridineResults[DAPILabel]
                  << ',' << FITCResults[DAPILabel]
                  << std::endl;
    }
  
    std::cout<<"Acridine holes..."<<std::endl;
    for (auto const& x : acridineHoleResults)
    {
        std::cout << x.first  // string (key)
                  << ':' 
                  << x.second // string's value 
                  << std::endl;
    }

    std::cout<<"FITC holes..."<<std::endl;
    for (auto const& x : FITCHoleResults)
    {
        std::cout << x.first  // string (key)
                  << ':' 
                  << x.second // string's value 
                  << std::endl;
    }
    
    myImage = DAPIImage;
    transformedImage = FITCImage;
  }
  else if(item == "Find islands"){
    std::cout<<"Testing islands..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);

    transformedImage -> medianFilter(3);
    transformedImage -> applyThreshold(60, 65);
    transformedImage -> fillHoles(90);
    
    // transformedImage -> dilateSymmetricSquare(3);
    // transformedImage -> errodeSymmetricSquare(3);
    
    transformedImage -> errodeSymmetricSquare(9);
    transformedImage -> dilateSymmetricSquare(9);
  }
  else if(item == "Find bad islands"){
    std::cout<<"Testing islands..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);

    transformedImage -> medianFilter(3);
    Image* denoisedImage = new Image(transformedImage);
    transformedImage -> applyThreshold(60, 65);
    transformedImage -> fillHoles(90);
    
    transformedImage -> errodeSymmetricSquare(9);
    transformedImage -> dilateSymmetricSquare(9);
    Image* islandImage = new Image(transformedImage);
    Image* circleIslandTestImage = new Image(transformedImage);
    Image* rectangleIslandTestImage = new Image(transformedImage);

    circleIslandTestImage -> errodeCircle(7);
    rectangleIslandTestImage -> errodeRectangle(19, 16);

    std::map<int, bool> circleIslandResults = circleIslandTestImage -> testShapes(4);  // map of label to good/bad
    std::map<int, bool> rectangleIslandResults = rectangleIslandTestImage -> testShapes(4);
    
    long* islandLabelMap = islandImage -> getLabelMap(255);
    std::set<int> islands = islandImage -> getUniqueLabels(islandLabelMap);
    std::map<int, bool> islandCombinedResults;
    int totalIslands = 0;
    int badIslands = 0;
    int expectedIslands = 20;

    for (auto const& circleIslandResult : circleIslandResults){
      islandCombinedResults[islandLabelMap[circleIslandResult.first]] = circleIslandResult.second;
    }
    for (auto const& rectangleIslandResult : rectangleIslandResults){
      islandCombinedResults[islandLabelMap[rectangleIslandResult.first]] =
        islandCombinedResults[islandLabelMap[rectangleIslandResult.first]] |
        rectangleIslandResult.second;
    }
    for (auto const& island : islands){
      int yIndex = floor(island / denoisedImage -> getWidth());
      int xIndex = island - yIndex * denoisedImage -> getWidth();
      std::cout<<
        xIndex << "," <<
        yIndex << "," <<
        islandCombinedResults[island] <<std::endl;
      if(island >= 0 & !islandCombinedResults[island]) {
        badIslands += 1;
      }
      if(island >= 0) {
        totalIslands += 1;
      }
    }
    std::cout<<"Bad islands: "<<badIslands<<std::endl;
    std::cout<<"Missing islands: "<<expectedIslands - totalIslands<<std::endl;

    Image* resultImage = new Image(denoisedImage);
    resultImage -> setChannels(3);
    resultImage -> createCanvas();

    // create three channels of data, populate based on results
    long pixelIndex;
    for(pixelIndex = 0; pixelIndex < denoisedImage -> getImageIndexCount(); pixelIndex++) {
      if(islandLabelMap[pixelIndex] == -1) {
        resultImage -> setPixelwChannel(pixelIndex, 0, denoisedImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 1, denoisedImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 2, denoisedImage -> getPixel(pixelIndex));
      }
      else if (islandCombinedResults[islandLabelMap[pixelIndex]]) {
        resultImage -> setPixelwChannel(pixelIndex, 0, 0);  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, denoisedImage -> getPixel(pixelIndex));  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
      else {
        resultImage -> setPixelwChannel(pixelIndex, 0, denoisedImage -> getPixel(pixelIndex));  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, 0);  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
    }
    transformedImage = resultImage;
    
  }
  else if(item == "Find bad holes"){
    std::cout<<"Testing islands..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);

    transformedImage -> medianFilter(3);
    Image* denoisedImage = new Image(transformedImage);
    Image* holeImage = new Image(denoisedImage);
    Image* islandImage = new Image(transformedImage);

    islandImage -> applyThreshold(60, 65);
    islandImage -> fillHoles(90);
    islandImage -> errodeSymmetricSquare(9);
    islandImage -> dilateSymmetricSquare(9);

    holeImage -> applyThreshold(200, 255);
    std::vector<std::tuple<float, float>> holeCenters = holeImage -> getCenters();
    std::vector<std::tuple<float, float>> islandCenters = islandImage -> getCenters();
    long* islandLabelMap = islandImage -> getLabelMap(255);

    int totalNotFound = 0;
    std::map<int, bool> centeringResults;
    for (auto const& holeCenter : holeCenters){
      long holeLabel = islandLabelMap[(int)(round(std::get<0>(holeCenter)) + islandImage -> getWidth() * round(std::get<1>(holeCenter)))];
      bool foundIsland = false;
      for (auto const& islandCenter : islandCenters){
        long islandLabel = islandLabelMap[(int)(round(std::get<0>(islandCenter)) + islandImage -> getWidth() * round(std::get<1>(islandCenter)))];
        if(holeLabel == islandLabel) {
          float distance = sqrt(pow((float)std::get<0>(holeCenter)-std::get<0>(islandCenter), 2) +
            pow((float)std::get<1>(holeCenter)-std::get<1>(islandCenter), 2));
          std::cout<<"Island Label: "<<islandLabel<<" Distance: "<<distance<<std::endl;
          if(distance > 0.5) {
            centeringResults[holeLabel] = false;
          }
          else {
            centeringResults[holeLabel] = true;
          }

          foundIsland = true;
          break;
        }
      }
      if(!foundIsland){
        totalNotFound += 1;
      }
    }
    std::cout<<"Islands not found for holds: "<<totalNotFound<<std::endl;
    Image* resultImage = new Image(denoisedImage);
    resultImage -> setChannels(3);
    resultImage -> createCanvas();

    // create three channels of data, populate based on results
    long pixelIndex;
    for(pixelIndex = 0; pixelIndex < denoisedImage -> getImageIndexCount(); pixelIndex++) {
      if(islandLabelMap[pixelIndex] == -1) {
        resultImage -> setPixelwChannel(pixelIndex, 0, denoisedImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 1, denoisedImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 2, denoisedImage -> getPixel(pixelIndex));
      }
      else if (centeringResults[islandLabelMap[pixelIndex]]) {
        resultImage -> setPixelwChannel(pixelIndex, 0, 0);  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, denoisedImage -> getPixel(pixelIndex));  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
      else {
        resultImage -> setPixelwChannel(pixelIndex, 0, denoisedImage -> getPixel(pixelIndex));  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, 0);  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
    }
    transformedImage = resultImage;
  }
  else if(item == "Find breaks"){
    std::cout<<"Testing islands..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);

    transformedImage -> medianFilter(3);
    Image* denoisedImage = new Image(transformedImage);
    Image* islandConnectorImage = new Image(transformedImage);
    Image* wireImage = new Image(transformedImage);

    islandConnectorImage -> applyThreshold(60, 65);
    islandConnectorImage -> fillHoles(90);
    islandConnectorImage -> errodeSymmetricSquare(5);
    islandConnectorImage -> dilateSymmetricSquare(5);

    wireImage -> applyThreshold(100, 200);
    wireImage -> invertIntensity();

    long* connectorLabelMap = islandConnectorImage -> getLabelMap(255);
    std::set<int> connectorLabels = islandConnectorImage -> getUniqueLabels(connectorLabelMap);

    long* wireLabelMap = wireImage -> getLabelMap(255);
    std::set<int> wireLabels = wireImage -> getUniqueLabels(wireLabelMap);

    std::map<long, long> connectorCounts;
    for (auto const& connectorLabel : connectorLabels){
      connectorCounts[wireLabelMap[connectorLabel]]++;
    }
    int brokenSegments = 0;
    for (auto const& connectorCount : connectorCounts){
      if(connectorCount.second < 2 and connectorLabelMap[connectorCount.first] == -1){
        brokenSegments++;
      }
    }
    std::cout<<"Total broken segments: "<<brokenSegments<<std::endl;

    // long* labelMap = connectionImage -> getLabelMap(255);
    // std::map<int, int> labelAreas = connectionImage -> getLabelAreas(labelMap);

    // int maxArea = 0;

    // for (auto const& labelArea : labelAreas){
    //   if(labelArea.first != -1){
    //     maxArea = std::max(maxArea, labelArea.second);
    //   }
    // }

    // std::cout<<"Max area is: "<<maxArea<<std::endl;

    Image* resultImage = new Image(denoisedImage);
    resultImage -> setChannels(3);
    resultImage -> createCanvas();
    

    // create three channels of data, populate based on results
    long pixelIndex;
    for(pixelIndex = 0; pixelIndex < denoisedImage -> getImageIndexCount(); pixelIndex++) {
      if(wireLabelMap[pixelIndex] == -1 | connectorLabelMap[pixelIndex] != -1) {
        resultImage -> setPixelwChannel(pixelIndex, 0, denoisedImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 1, denoisedImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 2, denoisedImage -> getPixel(pixelIndex));
      }
      else if (connectorCounts[wireLabelMap[pixelIndex]] >= 2) {
        resultImage -> setPixelwChannel(pixelIndex, 0, 0);  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, denoisedImage -> getPixel(pixelIndex));  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
      else {
        resultImage -> setPixelwChannel(pixelIndex, 0, denoisedImage -> getPixel(pixelIndex));  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, 0);  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
    }
    transformedImage = resultImage;
    // transformedImage = wireImage;
  }
  else if(item == "Check bottles"){
    std::cout<<"Testing islands..."<<std::endl;
    QFileDialog dialog(this, tr("Open File"));
    QString filename = dialog.getOpenFileName(this, "Select image to transform");
    std::cout<<"Opening: "<<filename.toStdString()<<std::endl;
    myImage = new Image(filename.toStdString());
    transformedImage = new Image(myImage);
    transformedImage -> applyThreshold(10, 200);
    transformedImage -> dilateSymmetricSquare(17);
    transformedImage -> errodeSymmetricSquare(17);
    transformedImage -> errodeSymmetricSquare(17);
    transformedImage -> dilateSymmetricSquare(17);

    long* bottleMap = transformedImage -> getLabelMap(255);
    std::set<int> bottleLabels = transformedImage -> getUniqueLabels(bottleMap);
    std::map<int, bool> bottleResults;
    for (auto const& bottleLabel : bottleLabels){
      if(bottleLabel != -1){
        bottleResults[bottleLabel] = false;
      }
    }
    long pixelIndex;
    long maxValidPixel = 78 * transformedImage -> getWidth();
    for(pixelIndex = 0; pixelIndex < maxValidPixel; pixelIndex++) {
      if(bottleMap[pixelIndex] != -1){
        bottleResults[bottleMap[pixelIndex]] = true;
      }
    }

    Image* resultImage = new Image(myImage);
    resultImage -> makeThreeChannel();

    for(pixelIndex = 0; pixelIndex < transformedImage -> getImageIndexCount(); pixelIndex++) {
      if(bottleMap[pixelIndex] == -1) {
        resultImage -> setPixelwChannel(pixelIndex, 0, myImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 1, myImage -> getPixel(pixelIndex));
        resultImage -> setPixelwChannel(pixelIndex, 2, myImage -> getPixel(pixelIndex));
      }
      else if (bottleResults[bottleMap[pixelIndex]]) {
        resultImage -> setPixelwChannel(pixelIndex, 0, 0);  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, myImage -> getPixel(pixelIndex));  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
      else {
        resultImage -> setPixelwChannel(pixelIndex, 0, myImage -> getPixel(pixelIndex));  // red
        resultImage -> setPixelwChannel(pixelIndex, 1, 0);  // green
        resultImage -> setPixelwChannel(pixelIndex, 2, 0);  // blue
      }
    }
    resultImage -> drawHorizontalLine(78);
    transformedImage = resultImage;

    myImage -> makeThreeChannel();
    myImage -> drawHorizontalLine(78);
    
  }
  else {
    std::cout<<"No menu item matched..."<<std::endl;
  }

  if (saveFileDialogResult == QMessageBox::Save){
    saveImage(transformedImage);
    // saveImage(myImage);
  }
  showLeftImage(myImage);
  showRightImage(transformedImage);
}