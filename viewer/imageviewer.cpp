#include "qtimageviewer.hpp"

#include <iostream>
#include <QApplication>

int main(int argc, char** argv){

  // Start Qt framework
  QApplication app( argc, argv );

  QtImageViewer* imv = new QtImageViewer();
  // Optionally give image on command line
  if(argc >= 2){
    QString filename(argv[1]);
    std::cout<<"Load directly: "<<filename.toStdString()<<std::endl;
    imv->showFile(filename);
  }
  // Draw our GUI
  imv->show();
  imv->resize(1000,600);

  // Run Qt application until someone presses quit
  int ret = app.exec();

  delete(imv);
  return ret;
}
