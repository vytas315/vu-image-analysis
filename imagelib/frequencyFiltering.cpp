#include <fftw3.h>
#include <iostream>
#include <math.h>

#include "image.hpp"

void ImageFloat::applyLowPassIdealFilter(double radius) {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  multiplyLowPassIdealFilter(radius, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::multiplyLowPassIdealFilter(double radius, fftw_complex* imageFreqDomain) {
  std::cout<<"Filtering with radius: "<<radius<<std::endl;
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      uint32 pixelIndex = v * getWidth() + u;
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 0;
      if (distance <= radius) {
        h_value = 1;
      }
      imageFreqDomain[pixelIndex][0] = imageFreqDomain[pixelIndex][0] * h_value;
      imageFreqDomain[pixelIndex][1] = imageFreqDomain[pixelIndex][1] * h_value;
    }
  }
}

void ImageFloat::applyHighPassIdealFilter(double radius) {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  multiplyHighPassIdealFilter(radius, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::multiplyHighPassIdealFilter(double radius, fftw_complex* imageFreqDomain) {
  std::cout<<"Filtering with radius: "<<radius<<std::endl;
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      uint32 pixelIndex = v * getWidth() + u;
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 0;
      if (distance > radius) {
        h_value = 1;
      }
      imageFreqDomain[pixelIndex][0] = imageFreqDomain[pixelIndex][0] * h_value;
      imageFreqDomain[pixelIndex][1] = imageFreqDomain[pixelIndex][1] * h_value;
    }
  }
}

void ImageFloat::applyLowPassButterworthFilter(double radius, double power) {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  multiplyLowPassButterworthFilter(radius, power, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::multiplyLowPassButterworthFilter(double radius, double power, fftw_complex* imageFreqDomain) {
  std::cout<<"Filtering with radius: "<<radius<<std::endl;
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      uint32 pixelIndex = v * getWidth() + u;
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 1.0 / (1.0 + pow(distance / radius, 2 * power));
      imageFreqDomain[pixelIndex][0] = imageFreqDomain[pixelIndex][0] * h_value;
      imageFreqDomain[pixelIndex][1] = imageFreqDomain[pixelIndex][1] * h_value;
    }
  }
}

void ImageFloat::applyHighPassButterworthFilter(double radius, double power) {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  multiplyHighPassButterworthFilter(radius, power, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::multiplyHighPassButterworthFilter(double radius, double power, fftw_complex* imageFreqDomain) {
  std::cout<<"Filtering with radius: "<<radius<<std::endl;
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      uint32 pixelIndex = v * getWidth() + u;
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 1.0 - 1.0 / (1.0 + pow(distance / radius, 2 * power));
      imageFreqDomain[pixelIndex][0] = imageFreqDomain[pixelIndex][0] * h_value;
      imageFreqDomain[pixelIndex][1] = imageFreqDomain[pixelIndex][1] * h_value;
    }
  }
}

void ImageFloat::applyLowPassGaussianFilter(double radius) {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  multiplyLowPassGaussianFilter(radius, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::multiplyLowPassGaussianFilter(double radius, fftw_complex* imageFreqDomain) {
  std::cout<<"Filtering with radius: "<<radius<<std::endl;
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      uint32 pixelIndex = v * getWidth() + u;
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = exp(-pow(distance, 2) / (2 * pow(radius, 2)));
      imageFreqDomain[pixelIndex][0] = imageFreqDomain[pixelIndex][0] * h_value;
      imageFreqDomain[pixelIndex][1] = imageFreqDomain[pixelIndex][1] * h_value;
    }
  }
}

void ImageFloat::applyHighPassGaussianFilter(double radius) {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  multiplyHighPassGaussianFilter(radius, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::multiplyHighPassGaussianFilter(double radius, fftw_complex* imageFreqDomain) {
  std::cout<<"Filtering with radius: "<<radius<<std::endl;
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      uint32 pixelIndex = v * getWidth() + u;
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 1 - exp(-pow(distance, 2) / (2 * pow(radius, 2)));
      imageFreqDomain[pixelIndex][0] = imageFreqDomain[pixelIndex][0] * h_value;
      imageFreqDomain[pixelIndex][1] = imageFreqDomain[pixelIndex][1] * h_value;
    }
  }
}

void ImageFloat::renderLowPassIdealFilter(double radius) {
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 0;
      if (distance <= radius) {
        h_value = 1;
      }
      setPixel(u, v, 1, 255 * h_value);
    }
  }
}

void ImageFloat::renderHighPassIdealFilter(double radius) {
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 0;
      if (distance > radius) {
        h_value = 1;
      }
      setPixel(u, v, 1, 255 * h_value);
    }
  }
}

void ImageFloat::renderLowPassButterworthFilter(double radius, double power) {
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 1.0 / (1.0 + pow(distance / radius, 2 * power));
      setPixel(u, v, 1, 255 * h_value);
    }
  }
}

void ImageFloat::renderHighPassButterworthFilter(double radius, double power) {
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 1.0 - 1.0 / (1.0 + pow(distance / radius, 2 * power));
      setPixel(u, v, 1, 255 * h_value);
    }
  }
}

void ImageFloat::renderLowPassGaussianFilter(double radius) {
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = exp(-pow(distance, 2) / (2 * pow(radius, 2)));      
      setPixel(u, v, 1, 255 * h_value);
    }
  }
}

void ImageFloat::renderHighPassGaussianFilter(double radius) {
  for (uint32 v = 0; v < getHeight(); v++) {
    for (uint32 u = 0; u < getWidth(); u++) {
      float distance = sqrt(pow(u - getWidth()/2.0, 2) + pow(v - getHeight()/2.0, 2));
      float h_value = 1 - exp(-pow(distance, 2) / (2 * pow(radius, 2)));
      setPixel(u, v, 1, 255 * h_value);
    }
  }
}
