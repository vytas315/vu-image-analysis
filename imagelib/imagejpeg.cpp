#include "image.hpp"

#include <iostream>

#include <stdlib.h>
#include <jpeglib.h>
#include <setjmp.h>

struct my_error_mgr {
  struct jpeg_error_mgr pub;

  jmp_buf setjmp_buffer;
};

typedef struct my_error_mgr *my_error_ptr;

METHODDEF(void)
my_error_exit(j_common_ptr cinfo)
{
  my_error_ptr myerr = (my_error_ptr)cinfo->err;
  (*cinfo->err->output_message) (cinfo);

  longjmp(myerr->setjmp_buffer, 1);
}


bool Image::loadJpeg(std::string filename){

  std::cout<<"Loading jpeg: "<<filename<<std::endl;
  struct jpeg_decompress_struct cinfo;
  return do_read_JPEG_file(&cinfo, (char*)filename.c_str());
}

int Image::do_read_JPEG_file(struct jpeg_decompress_struct *cinfo, char *filename)
{
struct my_error_mgr jerr;
  FILE *infile;                
  JSAMPARRAY buffer;           
  int row_stride;              

  if ((infile = fopen(filename, "rb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    return 0;
  }
  cinfo->err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  if (setjmp(jerr.setjmp_buffer)) {
    jpeg_destroy_decompress(cinfo);
    fclose(infile);
    return 0;
  }
  jpeg_create_decompress(cinfo);
  jpeg_stdio_src(cinfo, infile);

  (void)jpeg_read_header(cinfo, TRUE);
  (void)jpeg_start_decompress(cinfo);
  row_stride = cinfo->output_width * cinfo->output_components;
  buffer = (*cinfo->mem->alloc_sarray)
                ((j_common_ptr)cinfo, JPOOL_IMAGE, row_stride, 1);
  long image_byte_count = cinfo->output_width * cinfo->output_height * cinfo->output_components;
  _width = cinfo->output_width;
	_height = cinfo->output_height;
	_depth = 1;
	_channels = cinfo->output_components;
  _bpc = 8;  // TODO: read this from tags

  createCanvas();
  while (cinfo->output_scanline < cinfo->output_height) {
    (void)jpeg_read_scanlines(cinfo, buffer, 1);
    long buffer_byte_index = 0;
    for (buffer_byte_index = 0; buffer_byte_index < _width * _channels; buffer_byte_index++) {
      long image_index = (cinfo->output_scanline - 1) * row_stride +  buffer_byte_index;  // scanline is 1 indexed
      if (image_index > image_byte_count - 1) {
        std::cout<<"Jpeg index outside image: "<<image_index<<std::endl;
        std::cout<<"Line index: "<<cinfo->output_scanline<<std::endl;
        std::cout<<"Line byte index: "<<buffer_byte_index<<std::endl;
        std::cout<<"Image size: "<<image_byte_count<<std::endl;
        std::cout<<"Image width: "<<_width<<std::endl;
        std::cout<<"Image height: "<<_height<<std::endl;
        std::cout<<"Image channels: "<<_channels<<std::endl;
        return 0;
      }
      setPixel(image_index, *(buffer[0] + buffer_byte_index));
    }
  }
  (void)jpeg_finish_decompress(cinfo);
  jpeg_destroy_decompress(cinfo);
  fclose(infile);
  std::cout<<"Data size: "<<image_byte_count<<std::endl;
  std::cout<<"Width: "<<_width<<std::endl;
  std::cout<<"Height: "<<_height<<std::endl;
  std::cout<<"Depth: "<<_depth<<std::endl;
  std::cout<<"Channels: "<<_channels<<std::endl;
  return 1;
};
