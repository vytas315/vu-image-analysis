#include <cstring>
#include "image.hpp"

#include <iostream>
#include <tiffio.h>

bool readTiffMetaData(TIFF *tiff) {
  TIFFSetDirectory(tiff, 0);
  unsigned int width{0};
  TIFFGetField(tiff, TIFFTAG_TILEWIDTH, &width);
  return true;
}

bool Image::loadTiffTiled(TIFF* tiff, unsigned long data_size) {
  std::cout<<"Loading tiled tiff..."<<std::endl;
  uint32 imageWidth, imageLength;
	uint32 tileWidth, tileLength;
	uint32 x, y, xi, yi, buffer_index, channel, tiles_read, total_channels;
	unsigned char* buffer_data;
  tsize_t buf_size;
  tiles_read = 0;
  createCanvas();

	TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &imageWidth);
	TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &imageLength);
	TIFFGetField(tiff, TIFFTAG_TILEWIDTH, &tileWidth);
	TIFFGetField(tiff, TIFFTAG_TILELENGTH, &tileLength);
  TIFFGetField(tiff, TIFFTAG_SAMPLESPERPIXEL, &total_channels);
	buffer_data = (unsigned char*)_TIFFmalloc(TIFFTileSize(tiff));
	for (y = 0; y < imageLength; y += tileLength) {
	    for (x = 0; x < imageWidth; x += tileWidth) {
		    buf_size = TIFFReadTile(tiff, buffer_data, x, y, 0, 0);
        buffer_index = 0;
        for (yi = y; yi < y + tileLength; yi++) {
          for (xi = x; xi < x + tileWidth; xi++) {
            for (channel = 0; channel < total_channels; channel++) {
              int image_index = (yi * imageWidth + xi) * total_channels + channel;
              if (xi < imageWidth && yi < imageLength) {
                if (image_index > data_size - 1) {
                  std::cout<<"Image index too large: "<<image_index<<std::endl;
                  std::cout<<"Buffer index was: "<<buffer_index<<std::endl;
                  std::cout<<"Tiles read: "<<tiles_read<<std::endl;
                  return false;
                }
                if (buffer_index > buf_size - 1) {
                  std::cout<<"Buffer index too large: "<<buffer_index<<std::endl;
                  return false;
                }
                setPixel(image_index, *(buffer_data + buffer_index));
              }
              buffer_index += 1;
            }
          }
        }
        tiles_read++;
      }
  }
  std::cout<<"Tiles read: "<<tiles_read<<std::endl;
  _TIFFfree(buffer_data);
  return true;
}

bool Image::loadTiffStrip(TIFF* tiff, unsigned long data_size) {
  std::cout<<"Loading striped tiff..."<<std::endl;
  tdata_t buf;
	tstrip_t strip;
  tsize_t buf_size;
  uint32 strip_size, total_strips;
  strip_size = TIFFStripSize(tiff);
  total_strips = TIFFNumberOfStrips(tiff);
	buf = _TIFFmalloc(strip_size);
  createCanvas();
  std::cout<<"Starting loop..."<<std::endl;
	for (strip = 0; strip < total_strips; strip++) {
    buf_size = TIFFReadEncodedStrip(tiff, strip, buf, (tsize_t) -1);
    int start_index = strip * strip_size;
    unsigned char* buffer_data = (unsigned char*)buf;
    uint32 bit = 0;
    for(bit = 0; bit < buf_size; bit++){
      int image_index = start_index + bit;
      if (image_index > data_size - 1) {
        std::cout<<"Image index too large: "<<image_index<<std::endl;
        std::cout<<"Strip index was: "<<strip<<std::endl;
        std::cout<<"Buffer size was: "<<buf_size<<std::endl;
        std::cout<<"Total strips was: "<<total_strips<<std::endl;
        std::cout<<"Strip size was: "<<strip_size<<std::endl;
        std::cout<<"Bit index was: "<<bit<<std::endl;
        return false;
      }
      setPixel(image_index, *(buffer_data + bit));
    }
  }
  _TIFFfree(buf);
  return true;
}

unsigned char* loadTiffScanline(TIFF* tiff) {
  return nullptr;
}

bool Image::loadTiff(std::string filename){
  std::cout<<"Loading tif: "<<filename<<std::endl;

  TIFF *tiff = TIFFOpen(filename.c_str(),"r");
  // Read image meta data, height, width etc.
  readTiffMetaData(tiff);
  uint32 imageWidth, imageLength, imageDepth, total_channels;
  TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &imageWidth);
	TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &imageLength);
  TIFFGetField(tiff, TIFFTAG_IMAGEDEPTH, &imageDepth);
  TIFFGetField(tiff, TIFFTAG_SAMPLESPERPIXEL, &total_channels);
  
  _width = imageWidth;
	_height = imageLength;
	_depth = imageDepth;
	_channels = total_channels;
  _bpc = 8;  // TODO: read from tags

  int data_size = _width * _height * _channels;

  if(TIFFIsTiled(tiff)){
    loadTiffTiled(tiff, data_size);
  }
  else
    loadTiffStrip(tiff, data_size);
      // or    _data = loadTiffScanline(tiff);
  TIFFClose(tiff);
  std::cout<<"Data size: "<<data_size<<std::endl;
  std::cout<<"Width: "<<_width<<std::endl;
  std::cout<<"Height: "<<_height<<std::endl;
  std::cout<<"Depth: "<<_depth<<std::endl;
  std::cout<<"Channels: "<<_channels<<std::endl;
  return true;
};

bool Image::saveTiff(std::string filename){
  TIFF *out= TIFFOpen(filename.c_str(), "w");
  int sampleperpixel = getChannels();
  TIFFSetField (out, TIFFTAG_IMAGEWIDTH, _width);  // set the width of the image
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, _height);    // set the height of the image
  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, sampleperpixel);   // set number of channels per pixel
  TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);    // set the size of the channels
  TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT);    // set the origin of the image.
  //   Some other essential fields to set that you do not have to understand for now.
  TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  if (sampleperpixel == 3){
    TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
  }
  else {
    TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
  }
  tsize_t linebytes = sampleperpixel * _width;
  unsigned char *buf = NULL;
  if (TIFFScanlineSize(out) < linebytes)
      buf =(unsigned char *)_TIFFmalloc(linebytes);
  else
      buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));

  // We set the strip size of the file to be size of one row of pixels
  TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out, _width*sampleperpixel));

  //Now writing image to the file one strip at a time
  for (uint32 row = 0; row < _height; row++)
  {
      memcpy(buf, &_data[(_height-row-1)*linebytes], linebytes);    // check the index here, and figure out why not using h*linebytes
      if (TIFFWriteScanline(out, buf, row, 0) < 0)
      break;
  }
  (void) TIFFClose(out);
  if (buf)
      _TIFFfree(buf);
  return true;
}
