#include <cmath>

#include "image.hpp"

void ImageFloat::generateImageA(){
  // Low frequency horizontal
  setWidth(512);
  setHeight(512);
  createCanvas();
  float alphaX = 2.0f*M_PI / static_cast<float>(getWidth());;
  float alphaY = 2.0f*M_PI / static_cast<float>(getHeight());;
  for(unsigned int yIter = 0; yIter < getHeight(); yIter++){
    for(unsigned int xIter = 0; xIter < getWidth(); xIter++){
      float i = 255.0f*fabs(cos( 1.0f* alphaX * static_cast<float>(xIter) + 0.0f* alphaY * static_cast<float>(yIter)));
      setPixel(xIter,yIter, 1, i);
    }
  }
}

void ImageFloat::generateImageB(){
  // Low frequency vertical
  setWidth(512);
  setHeight(512);
  createCanvas();
  float alphaX = 2.0f*M_PI / static_cast<float>(getWidth());;
  float alphaY = 2.0f*M_PI / static_cast<float>(getHeight());;
  for(unsigned int yIter = 0; yIter < getHeight(); yIter++){
    for(unsigned int xIter = 0; xIter < getWidth(); xIter++){
      float i = 255.0f*fabs(cos( 0.0f* alphaX * static_cast<float>(xIter) + 1.0f* alphaY * static_cast<float>(yIter)));
      setPixel(xIter,yIter, 1, i);
    }
  }
}

void ImageFloat::generateImageC(){
  // Low frequency diagonal
  setWidth(512);
  setHeight(512);
  createCanvas();
  float alphaX = 2.0f*M_PI / static_cast<float>(getWidth());;
  float alphaY = 2.0f*M_PI / static_cast<float>(getHeight());;
  for(unsigned int yIter = 0; yIter < getHeight(); yIter++){
    for(unsigned int xIter = 0; xIter < getWidth(); xIter++){
      float i = 255.0f*fabs(cos( 1.0f* alphaX * static_cast<float>(xIter) + 1.0f* alphaY * static_cast<float>(yIter)));
      setPixel(xIter,yIter, 1, i);
    }
  }
}

void ImageFloat::generateImageD(){
  // High frequency horizontal
  setWidth(512);
  setHeight(512);
  createCanvas();
  float alphaX = 20.0f*M_PI / static_cast<float>(getWidth());;
  float alphaY = 20.0f*M_PI / static_cast<float>(getHeight());;
  for(unsigned int yIter = 0; yIter < getHeight(); yIter++){
    for(unsigned int xIter = 0; xIter < getWidth(); xIter++){
      float i = 255.0f*fabs(
        cos( 1.0f* alphaX * static_cast<float>(xIter) + 0.0f* alphaY * static_cast<float>(yIter)));
      setPixel(xIter,yIter, 1, i);
    }
  }
}

void ImageFloat::generateImageE(){
  setWidth(512);
  setHeight(512);
  createCanvas();
  float alphaX = 2.0f*M_PI / static_cast<float>(getWidth());;
  float alphaY = 2.0f*M_PI / static_cast<float>(getHeight());;
  float alphaX2 = 0.5f*M_PI / static_cast<float>(getWidth());;
  float alphaY2 = 0.5f*M_PI / static_cast<float>(getHeight());;
  for(unsigned int yIter = 0; yIter < getHeight(); yIter++){
    for(unsigned int xIter = 0; xIter < getWidth(); xIter++){
      float i = 255.0f*fabs(
        cos( 1.0f* alphaX * static_cast<float>(xIter) + 0.0f* alphaY * static_cast<float>(yIter)) +
        cos( 1.0f* alphaX2 * static_cast<float>(xIter) + 0.0f* alphaY2 * static_cast<float>(yIter)));
      setPixel(xIter,yIter, 1, i);
    }
  }
}
