#ifndef SIMPLE_IMAGE_LIBRARY_HPP
#define SIMPLE_IMAGE_LIBRARY_HPP

#include <string>
#include <set>
#include <eigen3/Eigen/Core> // access to vectors and matrices
#include <eigen3/Eigen/LU>
#include <fftw3.h>
#include <tiffio.h>
#include <map>
#include <vector>

class Image{
public:
	Image();
	Image(std::string filename);
	Image(std::string filename1, std::string filename2, std::string filename3);
	Image(long width, long height, long channels, long bpc, long depth);
	Image(Image* originalImage);
	bool saveOutput(std::string filename);
	virtual ~Image();
	bool createCanvas();
	void makeThreeChannel();
	// File related
	bool openFile(std::string filename);
	bool combineFiles(std::string filename1, std::string filename2, std::string filename3);
	// Transform related
	bool transformImage();
	bool applyPowerTransform();
	bool applyPowerTransform(float gamma);
	bool applyPieceWiseLinearTransform();
	bool applyHistogramNormalization();
	void applyThreshold(unsigned char thresholdValue);
	void applyThreshold(unsigned char min, unsigned char max);
	void invertIntensity();
	void drawHorizontalLine(int yValue);
	// Fourier Transforms
	bool fourierPad();
	bool fourierCrop();
	// Image related
	unsigned char* getImageData();
	bool setPixel(long pixelIndex, unsigned char pixelValue);
	bool setPixelwChannel(long pixelIndex, long channel, unsigned char pixelValue);
	bool setPixel(int x, int y, long channel, unsigned char pixelValue);
	unsigned char getPixel(long pixelIndex);
	unsigned char getPixel(int x, int y, long channel);
	unsigned char getPixel(int x, int y);
	// Get attributes
	unsigned long getWidth();
	unsigned long getHeight();
	unsigned long getDepth();
	unsigned long getChannels();
	unsigned long getBPC();
	unsigned long getImageSize();
	unsigned long getImageIndexCount();
	void setChannels(long channels);
	bool getHistogram(unsigned long (&histogram)[256]);
	// Count
	long* countConnected();
	long* getLabelMap(unsigned char intensity);
	std::set<int> getUniqueLabels(unsigned char intensity);
	std::set<int> getUniqueLabels(long* labelMap);
	std::vector<std::tuple<float, float>> getCenters();
	std::map<int, int> getLabelAreas(long* labelMap);
	std::map<int, int> getLabelAreas();
	// Morphology
	void errodeSymmetricSquare(int size);
	void dilateSymmetricSquare(int size);
	void errodeRectangle(int width, int height);
	void errodeCircle(int radius);
	void fillHoles(int maxSize);
	std::map<int, bool> testShapes(int tolerance);
	// Filter
	void adaptiveMedianNoiseReduction(int sMin, int sMax);
	void medianFilter(int size);
protected:
  bool isTiffFile(std::string filename);
	bool isJpegFile(std::string filename);
	std::string getExtension(std::string filename);
	unsigned long _width{0};
	unsigned long _height{0};
	unsigned long _depth{0};
	unsigned long _channels{0};
	unsigned long _bpc{0};
	unsigned char _backgroundColor{0};
	fftw_complex *inComplex, *outComplex, *fourierComplex;

	unsigned char* _data{nullptr};

	struct boundingBox {
    float xmin;
    float xmax;
    float ymin;
    float ymax;
	};
	// Image manipulation
	bool setWidth(unsigned long newWidth);
	bool setHeight(unsigned long newHeight);
	unsigned char getPixel(Eigen::Vector3f &idx, long channel, bool useBiLinear = false);
	unsigned char getNearestPixel(Eigen::Vector3f &idx, long channel);
	unsigned char getPixelBiLinear(Eigen::Vector3f &idx, long channel);
	bool replaceImage(Image newImage);
	unsigned char powerTransformPixel(unsigned char pixelIntensity, float gamma);
	unsigned char pieceWiseLinearTransformPixel(unsigned char pixelIntensity);
	// Tiff related stuff
	bool loadTiff(std::string filename);
	bool saveTiff(std::string filename);
	bool loadTiffTiled(TIFF* tiff, unsigned long data_size);
	bool loadTiffStrip(TIFF* tiff, unsigned long data_size);
	// Jpeg related stuff
	bool loadJpeg(std::string filename);
	int do_read_JPEG_file(struct jpeg_decompress_struct *cinfo, char *filename);
	// Transforms
	Image::boundingBox getTransformedBoundingBox(Eigen::Matrix3f transformMatrix);
	Image getNewImage(Image::boundingBox transformedBox);
	// Counting
	void _initializeLabels(long* labelMap);
	long _spreadConnected(long* labelMap, unsigned char intensity);
	long _countLabels(long* labelMap);
	long _getMaxLabel(long* labelMap, int xIndex, int yIndex);
	long _getIndex(int xIndex, int yIndex);
	// Morphology
	unsigned char _getErrodeSymmetricSquareValue(int xIndex, int yIndex, int size);
	unsigned char _getErrodeRectangleValue(int xIndex, int yIndex, int width, int height);
	unsigned char _getErrodeCircleValue(int xIndex, int yIndex, int radius);
	// Filtering
	unsigned char _getAdaptiveNoiseValue(int xIndex, int yIndex, int sSize, int sMax);
	unsigned char _getMedian(int xIndex, int yIndex, int size);
	// Utility
	unsigned char _median(int a[], int n);
};

class ImageFloat :
	public Image{
		public :
			ImageFloat(Image* originalImage);
			ImageFloat();
			float getPixel(int x, int y);
			void setPixel(int x, int y, float i);
			Image* getNormalizedTo8Bits();
			Image* getTruncatedTo8Bits();
			Image* getLogNormalizedTo8Bits();
			Image* getLogTruncatedTo8Bits();

			bool applyLaplacian(Image* originalImage);
			bool add(Image* originalImage);
			bool multiply(Image* originalImage);
			bool applySobel(Image* originalImage);
			bool apply3x3Smoothing(Image* originalImage);
			// Fourier
			bool fourierShift();
			void fourierTransform();
			void inverseFourierTransform();
			// Generated
			void generateImageA();
			void generateImageB();
			void generateImageC();
			void generateImageD();
			void generateImageE();
			// Filtering
			void applyLowPassIdealFilter(double radius);
			void applyHighPassIdealFilter(double radius);
			void applyLowPassButterworthFilter(double radius, double power);
			void applyHighPassButterworthFilter(double radius, double power);
			void applyLowPassGaussianFilter(double power);
			void applyHighPassGaussianFilter(double power);
			void renderLowPassIdealFilter(double radius);
			void renderHighPassIdealFilter(double radius);
			void renderLowPassButterworthFilter(double radius, double power);
			void renderHighPassButterworthFilter(double radius, double power);
			void renderLowPassGaussianFilter(double radius);
			void renderHighPassGaussianFilter(double radius);
		private:
			bool createCanvas();
			
			float* _data{nullptr};

			float getPixel(int x, int y, long channel);
			float getPixel(long pixelIndex);
			void setPixel(int x, int y, int channel, float intensityValue);
			void setPixel(long pixelIndex, float intensityValue);

			void populateComplexData(fftw_complex* complexData);
			void applyComplexData(fftw_complex* complexData);
			void applyFourierFunction(fftw_complex* inComplex, fftw_complex* outComplex);
			void applyInverseFourierFunction(fftw_complex* inComplex, fftw_complex* outComplex);
			void rectifyComplexData(fftw_complex* complexData);
			// Filtering
			void multiplyLowPassIdealFilter(double radius, fftw_complex* imageFreqDomain);
			void multiplyHighPassIdealFilter(double radius, fftw_complex* imageFreqDomain);
			void multiplyLowPassButterworthFilter(double radius, double power, fftw_complex* imageFreqDomain);
			void multiplyHighPassButterworthFilter(double radius, double power, fftw_complex* imageFreqDomain);
			void multiplyLowPassGaussianFilter(double radius, fftw_complex* imageFreqDomain);
			void multiplyHighPassGaussianFilter(double radius, fftw_complex* imageFreqDomain);
	};
#endif
