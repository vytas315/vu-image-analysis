#include <iostream>
#include <math.h>

#include "image.hpp"

unsigned char Image::powerTransformPixel(unsigned char pixelIntensity, float gamma){
  float c = 1;
  float normalizedPixelIntensity = pixelIntensity/255.0;
  return round(c * pow(normalizedPixelIntensity,gamma) * 255);
}

unsigned char Image::pieceWiseLinearTransformPixel(unsigned char pixelIntensity){
  float normalizedPixelIntensity = pixelIntensity/255.0;
  if(normalizedPixelIntensity < 0.1){
    // Lower intensities and decrease contrast for the first 30%
    return normalizedPixelIntensity / 3 * 255;
  } else if(normalizedPixelIntensity < 0.2){
    // Increase contrast for the middle 40%
    return (8 * normalizedPixelIntensity - 2.3f/3.0f) * 255;
  } else {
    // Increase intensities and lower contrast for upper 30%
    return (normalizedPixelIntensity / 3 + (2.3f/3.0f)) * 255;
  }

  //////////////////////////
  //  UNCOMMENT THIS CODE FOR THRESHOLDING
  /////////////////////////
  // if(normalizedPixelIntensity < 0.2){
  //   // Lower intensities and decrease contrast for the first 30%
  //   return (unsigned char)0;
  // } else {
  //   // Increase intensities and lower contrast for upper 30%
  //   return (unsigned char)255;
  // }
}

void Image::applyThreshold(unsigned char thresholdValue){
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageSize(); pixelIndex++) {
    if (getPixel(pixelIndex) >= thresholdValue) {
      setPixel(pixelIndex, (unsigned char) 255);
    }
    else {
      setPixel(pixelIndex, (unsigned char) 0);
    }
  }
}

void Image::applyThreshold(unsigned char min, unsigned char max){
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageSize(); pixelIndex++) {
    if (getPixel(pixelIndex) >= min & getPixel(pixelIndex) <= max) {
      setPixel(pixelIndex, (unsigned char) 255);
    }
    else {
      setPixel(pixelIndex, (unsigned char) 0);
    }
  }
}

bool Image::applyPowerTransform(){
  return applyPowerTransform(0.3);
}

bool Image::applyPowerTransform(float gamma){
  std::cout<<"Applying power law transform..."<<std::endl;
  unsigned char intensityMap[256];
  long intensityVal;
  for(intensityVal = 0; intensityVal < 255; intensityVal++){
    intensityMap[intensityVal] = powerTransformPixel(intensityVal, gamma);
  }
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageSize(); pixelIndex++) {
    setPixel(pixelIndex, intensityMap[getPixel(pixelIndex)]);
  }
  return true;
}

bool Image::applyPieceWiseLinearTransform(){
  std::cout<<"Applying piece wise linear transform..."<<std::endl;
  unsigned char intensityMap[256];
  long intensityVal;
  for(intensityVal = 0; intensityVal < 255; intensityVal++){
    intensityMap[intensityVal] = pieceWiseLinearTransformPixel(intensityVal);
  }
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageSize(); pixelIndex++) {
    setPixel(pixelIndex, intensityMap[getPixel(pixelIndex)]);
  }
  return true;
}

bool Image::applyHistogramNormalization(){
  unsigned long histogram[256] = {0};
  getHistogram(histogram);
  unsigned char intensityMap[256];
  long intensityVal;
  unsigned long cummulativePixels = 0;
  for(intensityVal = 0; intensityVal < 256; intensityVal++){
    cummulativePixels += histogram[intensityVal];
    intensityMap[intensityVal] = round((float)cummulativePixels / getImageSize() * 255);
  }
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageSize(); pixelIndex++) {
    setPixel(pixelIndex, intensityMap[getPixel(pixelIndex)]);
  }
  return true;
}

bool Image::getHistogram(unsigned long (&histogram)[256]){
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageSize(); pixelIndex++) {
    histogram[getPixel(pixelIndex)] = (long)histogram[getPixel(pixelIndex)] + 1;
  }
  return true;
}

void Image::invertIntensity() {
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    setPixel(pixelIndex, 255 - getPixel(pixelIndex));
  }
}

void Image::drawHorizontalLine(int yValue){
  int xIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++){
    long pixelIndex = yValue * getWidth() + xIndex;
    setPixelwChannel(pixelIndex, 0, 255);
    setPixelwChannel(pixelIndex, 1, 0);
    setPixelwChannel(pixelIndex, 2, 0);
  }
}
