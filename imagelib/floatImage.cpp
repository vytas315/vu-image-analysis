#include "image.hpp"

#include <iostream>
#include <math.h>

ImageFloat::ImageFloat(){
  _bpc = 32;
  _depth = 1;
  _channels = 1;
}

ImageFloat::ImageFloat(Image* originalImage){
  _height = originalImage->getHeight();
  _width = originalImage->getWidth();
  _channels = originalImage->getChannels();
  _bpc = 32; // 4 bytes, 32 bits per channel which is now a float
  _depth = originalImage->getDepth();
  createCanvas();
  int pixelIndex;
  for(pixelIndex = 0; pixelIndex < originalImage->getImageIndexCount(); pixelIndex++){
    setPixel(pixelIndex, float(originalImage->getPixel(pixelIndex)));
  }
}

Image* ImageFloat::getNormalizedTo8Bits(){
  // TODO: get max and min values. Scale to 8 bit intensities.
  float max = getPixel(0), min = getPixel(0);
  int pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    if(getPixel(pixelIndex) < min)
      min = getPixel(pixelIndex);
    if(getPixel(pixelIndex) > max)
      max = getPixel(pixelIndex);
  }
  float range = max - min;
  std::cout<<"Min: "<<min<<std::endl;
  std::cout<<"Max: "<<max<<std::endl;
  std::cout<<"Range: "<<range<<std::endl;
  Image* normalizedImage = new Image(_width, _height, _channels, _bpc/4, _depth);
  normalizedImage->createCanvas();
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    char scaledPixel = (char)((getPixel(pixelIndex)-min)/range * 255);
    normalizedImage->setPixel(pixelIndex, scaledPixel);
  }
  return normalizedImage;
}

Image* ImageFloat::getLogNormalizedTo8Bits(){
  // TODO: get max and min values. Scale to 8 bit intensities.
  float max = log(getPixel(0)), min = log(getPixel(0));
  int pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    if(log(getPixel(pixelIndex)) < min)
      min = log(getPixel(pixelIndex));
    if(log(getPixel(pixelIndex)) > max)
      max = log(getPixel(pixelIndex));
  }
  min = 0;
  float range = max - min;
  std::cout<<"Min: "<<min<<std::endl;
  std::cout<<"Max: "<<max<<std::endl;
  std::cout<<"Range: "<<range<<std::endl;
  Image* normalizedImage = new Image(_width, _height, _channels, _bpc/4, _depth);
  normalizedImage->createCanvas();
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    char scaledPixel = (char)((log(getPixel(pixelIndex))-min)/range * 255);
    normalizedImage->setPixel(pixelIndex, scaledPixel);
  }
  return normalizedImage;
}

Image* ImageFloat::getTruncatedTo8Bits(){
  // TODO: get max and min values. Scale to 8 bit intensities.
  float max = getPixel(0), min = getPixel(0);
  int pixelIndex;
  Image* normalizedImage = new Image(_width, _height, _channels, _bpc/4, _depth);
  normalizedImage->createCanvas();
  std::cout<<"First pixel value: "<<getPixel(0)<<std::endl;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    unsigned char scaledPixel;
    if (getPixel(pixelIndex) < 0) {
      scaledPixel = 0;
    }
    else if(getPixel(pixelIndex) > 255) {
      scaledPixel = 255;
    }
    else {
      scaledPixel = (unsigned char)getPixel(pixelIndex);
    }
    normalizedImage->setPixel(pixelIndex, scaledPixel);
  }
  std::cout<<"New image width: "<<normalizedImage->getWidth()<<std::endl;
  return normalizedImage;
}

Image* ImageFloat::getLogTruncatedTo8Bits(){
  // TODO: get max and min values. Scale to 8 bit intensities.
  float max = getPixel(0), min = getPixel(0);
  int pixelIndex;
  Image* normalizedImage = new Image(_width, _height, _channels, _bpc/4, _depth);
  normalizedImage->createCanvas();
  std::cout<<"First pixel value: "<<getPixel(0)<<std::endl;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    unsigned char scaledPixel;
    if (log(getPixel(pixelIndex)) < 0) {
      scaledPixel = 0;
    }
    else if(log(getPixel(pixelIndex)) > 255) {
      scaledPixel = 255;
    }
    else {
      scaledPixel = (unsigned char)log(getPixel(pixelIndex));
    }
    normalizedImage->setPixel(pixelIndex, scaledPixel);
  }
  std::cout<<"New image width: "<<normalizedImage->getWidth()<<std::endl;
  return normalizedImage;
}

void ImageFloat::setPixel(int x, int y, int channel, float intensityValue){
  if (x < 0 || x >= _width || y < 0 || y >= _height) {
    std::cout<<"Set pixel out of dimensions."<<std::endl;
  }
  long imageIndex = (y * (_width) + x) * _channels + channel;
  setPixel(imageIndex, intensityValue);
}

void ImageFloat::setPixel(long pixelIndex, float intensityValue){
  if(pixelIndex < 0 || pixelIndex >= getImageIndexCount()) {
    std::cout<<"Set pixel out of range: "<<pixelIndex<<std::endl;
  }
  _data[pixelIndex] = intensityValue;
}

float ImageFloat::getPixel(int x, int y, long channel){
  if (x < 0 || x >= _width || y < 0 || y >= _height) {
    return float(_backgroundColor);
  }
  long imageIndex = (y * (_width) + x) * _channels + channel;
  if(imageIndex < 0 || imageIndex >= getImageIndexCount()) {
    return float(_backgroundColor);
  }
  return getPixel(imageIndex);
}

float ImageFloat::getPixel(long pixelIndex){
  if(pixelIndex < 0 || pixelIndex >= getImageIndexCount()) {
    std::cout<<"Get pixel out of range: "<<pixelIndex<<std::endl;
    return float(_backgroundColor);
  }
  return _data[pixelIndex];
}

bool ImageFloat::createCanvas(){
  _data = new float[getImageSize()]{};
  return true;
}

// Convert to 8 bit image by normalizing and rounding
