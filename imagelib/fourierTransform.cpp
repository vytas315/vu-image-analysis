#include <complex>
#include <fftw3.h>
#include <iostream>
#include <math.h>

#include "image.hpp"




bool Image::fourierPad() {
  unsigned char* padded_image_data = new unsigned char[getImageSize() * 4]{};
  for (uint32 row = 0; row < getHeight(); row++) {
      memcpy(&padded_image_data[row * getWidth() * 2], &_data[row * getWidth()], getWidth());
  }
  delete(_data);
  _data = padded_image_data;
  _height = getHeight() * 2;
  _width = getWidth() * 2;
  return true;
}

bool Image::fourierCrop() {
  unsigned char* cropped_image_data = new unsigned char[getImageSize() / 4]{};
  for (uint32 row = 0; row < getHeight() / 2; row++) {
      memcpy(&cropped_image_data[row * getWidth() / 2], &_data[row * getWidth()], getWidth() / 2);
  }
  delete(_data);
  _data = cropped_image_data;
  _height = getHeight() / 2;
  _width = getWidth() / 2;
  return true;
}

bool ImageFloat::fourierShift() {
  for (uint32 x = 0; x < getWidth(); x++) {
    for (uint32 y = 0; y < getHeight(); y++) {
      setPixel(x, y, 0, getPixel(x, y, 0) * pow(-1, x + y));
    }
  }
  return true;
}

void ImageFloat::fourierTransform() {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, outComplex);
  rectifyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex);
}

void ImageFloat::inverseFourierTransform() {
  inComplex = fftw_alloc_complex(getImageIndexCount());
  fourierComplex = fftw_alloc_complex(getImageIndexCount());
  outComplex = fftw_alloc_complex(getImageIndexCount());
  populateComplexData(inComplex);
  applyFourierFunction(inComplex, fourierComplex);
  applyInverseFourierFunction(fourierComplex, outComplex);
  applyComplexData(outComplex);
  fftw_free(inComplex); fftw_free(outComplex); fftw_free(fourierComplex);
}

void ImageFloat::populateComplexData(fftw_complex* complexData) {
  std::cout<<"Populating complex data... "<<std::endl;
  for (uint32 pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    complexData[pixelIndex][0] = (double)_data[pixelIndex];
  }
}

void ImageFloat::applyFourierFunction(fftw_complex* inComplex, fftw_complex* outComplex) {
  std::cout<<"Applying fourier... "<<std::endl;
  int wd = getWidth();
  int ht = getHeight();
  fftw_plan p = fftw_plan_dft_2d(ht, wd, inComplex, outComplex, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
}

void ImageFloat::applyInverseFourierFunction(fftw_complex* inComplex, fftw_complex* outComplex) {
  std::cout<<"Applying fourier... "<<std::endl;
  int wd = getWidth();
  int ht = getHeight();
  fftw_plan p = fftw_plan_dft_2d(ht, wd, inComplex, outComplex, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
}

void ImageFloat::rectifyComplexData(fftw_complex* complexData) {
  std::cout<<"Rectifying data... "<<std::endl;
  for (uint32 pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    setPixel(pixelIndex, log(pow(complexData[pixelIndex][0], 2) + pow(complexData[pixelIndex][1], 2) + 0.0000001));
  }
}

void ImageFloat::applyComplexData(fftw_complex* complexData) {
  std::cout<<"Rectifying data... "<<std::endl;
  for (uint32 pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    setPixel(pixelIndex, complexData[pixelIndex][0]/getWidth()/getHeight());
  }
}
