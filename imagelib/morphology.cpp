#include <iostream>
#include <math.h>
#include <map>

#include "image.hpp"

void Image::errodeSymmetricSquare(int size) {
  // sum all pixels inside SE
  // compare to 255 * count of pixels
  // assign new value
  unsigned char errodedImageData[getImageIndexCount()]{0};
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      errodedImageData[_getIndex(xIndex, yIndex)] = _getErrodeSymmetricSquareValue(xIndex, yIndex, size);
    }
  }
  memcpy(_data, errodedImageData, _width * _height * _channels);
}

unsigned char Image::_getErrodeSymmetricSquareValue(int xIndex, int yIndex, int size){
  int seSize = 0;
  long seSum = 0;
  int seXindex, seYindex;
  for(seXindex = xIndex - (size - 1) / 2; seXindex <= xIndex + (size - 1) / 2; seXindex++) {
    for(seYindex = yIndex - (size - 1) / 2; seYindex <= yIndex + (size - 1) / 2; seYindex++) {
      if(seXindex >= 0 & seYindex >= 0 & seXindex < getWidth() & seYindex < getHeight()) {
        seSize += 1;
        seSum += getPixel(seXindex, seYindex);
      }
    }
  }
  if(seSum == seSize * 255) {
    return (unsigned char) 255;
  }
  return (unsigned char) 0;
}

void Image::dilateSymmetricSquare(int size) {
  // sum all pixels inside SE
  // compare to 255 * count of pixels
  // assign new value
  unsigned char dilatedImageData[getImageIndexCount()]{0};
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      if(getPixel(xIndex, yIndex) == 255) {
        int seXindex, seYindex;
        for(seXindex = xIndex - (size - 1) / 2; seXindex <= xIndex + (size - 1) / 2; seXindex++) {
          for(seYindex = yIndex - (size - 1) / 2; seYindex <= yIndex + (size - 1) / 2; seYindex++) {
            if(seXindex >= 0 & seYindex >= 0 & seXindex < getWidth() & seYindex < getHeight()) {
              dilatedImageData[_getIndex(seXindex, seYindex)] = 255;
            }
          }
        }
      }
    }
  }
  memcpy(_data, dilatedImageData, _width * _height * _channels);
}

void Image::fillHoles(int maxSize) {
  long* holeLabelMap = getLabelMap(0);
  std::map<long,long> holeSizeMap;
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    if(holeSizeMap.count(holeLabelMap[pixelIndex]) == 0) {
      holeSizeMap[holeLabelMap[pixelIndex]] = 1;
    }
    else {
      holeSizeMap[holeLabelMap[pixelIndex]] = holeSizeMap[holeLabelMap[pixelIndex]] + 1;
    }
  }
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    if(holeSizeMap[holeLabelMap[pixelIndex]] <= maxSize) {
      setPixel(pixelIndex, 255);
    }
  }
}

void Image::errodeRectangle(int width, int height) {
  // sum all pixels inside SE
  // compare to 255 * count of pixels
  // assign new value
  unsigned char errodedImageData[getImageIndexCount()]{0};
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      errodedImageData[_getIndex(xIndex, yIndex)] = _getErrodeRectangleValue(xIndex, yIndex, width, height);
    }
  }
  memcpy(_data, errodedImageData, _width * _height * _channels);
}

unsigned char Image::_getErrodeRectangleValue(int xIndex, int yIndex, int width, int height){
  int seSize = 0;
  long seSum = 0;
  int seXindex, seYindex;
  for(seXindex = xIndex - (width - 1) / 2; seXindex <= xIndex + (width - 1) / 2; seXindex++) {
    for(seYindex = yIndex - (height - 1) / 2; seYindex <= yIndex + (height - 1) / 2; seYindex++) {
      if(seXindex >= 0 & seYindex >= 0 & seXindex < getWidth() & seYindex < getHeight()) {
        seSize += 1;
        seSum += getPixel(seXindex, seYindex);
      }
    }
  }
  if(seSum == seSize * 255) {
    return (unsigned char) 255;
  }
  return (unsigned char) 0;
}

void Image::errodeCircle(int radius) {
  unsigned char errodedImageData[getImageIndexCount()]{0};
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      errodedImageData[_getIndex(xIndex, yIndex)] = _getErrodeCircleValue(xIndex, yIndex, radius);
    }
  }
  memcpy(_data, errodedImageData, _width * _height * _channels);
}

unsigned char Image::_getErrodeCircleValue(int xIndex, int yIndex, int radius){
  int seSize = 0;
  long seSum = 0;
  int seXindex, seYindex;
  int width = radius * 2 + 1;
  for(seXindex = xIndex - (width - 1) / 2; seXindex <= xIndex + (width - 1) / 2; seXindex++) {
    for(seYindex = yIndex - (width - 1) / 2; seYindex <= yIndex + (width - 1) / 2; seYindex++) {
      if(seXindex >= 0 & seYindex >= 0 & seXindex < getWidth() & seYindex < getHeight()) {
        float xDistance = abs(seXindex - xIndex);
        float yDistance = abs(seYindex - yIndex);
        float distance = sqrt(pow(xDistance, 2) + pow(yDistance, 2));
        if(distance <= radius) {
          seSize += 1;
          seSum += getPixel(seXindex, seYindex);
        }
      }
    }
  }
  if(seSum == seSize * 255) {
    return (unsigned char) 255;
  }
  return (unsigned char) 0;
}
