#include <iostream>
#include <math.h>
#include <set>
#include <map>
#include <eigen3/Eigen/Core>
#include <vector>

#include "image.hpp"

long* Image::countConnected() {
  std::cout<<"Counting connected..."<<std::endl;
  long* labelMap = getLabelMap(255);
  std::cout<<"Total labels = "<<_countLabels(labelMap)<<std::endl;
  return labelMap;
}

long* Image::getLabelMap(unsigned char intensity) {
  std::cout<<"Counting connected..."<<std::endl;
  long* labelMap = new long[getImageIndexCount()];
  _initializeLabels(labelMap);
  long changes = 1;
  long spreadCount = 0;
  while (changes > 0) {
    changes = _spreadConnected(labelMap, intensity);
    if (spreadCount % 10 == 0) {
      std::cout<<spreadCount<<": Changes spread = "<<changes<<std::endl;
      std::cout<<"Remaining labels = "<<_countLabels(labelMap)<<std::endl;
    }
    spreadCount += 1;
  }
  return labelMap;
}

std::set<int> Image::getUniqueLabels(unsigned char intensity) {
  std::cout<<"Counting connected..."<<std::endl;
  long* labelMap = getLabelMap(intensity);
  return getUniqueLabels(labelMap);
}

std::set<int> Image::getUniqueLabels(long* labelMap) {
  std::set<int> uniqueLabels(labelMap, labelMap + getImageIndexCount());
  return uniqueLabels;
}

void Image::_initializeLabels(long* labelMap) {
  std::cout<<"Initializing labels..."<<std::endl;
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    labelMap[pixelIndex] = pixelIndex;
  }
}

long Image::_spreadConnected(long* labelMap, unsigned char intensity) {
  long changes = 0;
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      if(getPixel(xIndex, yIndex) == intensity) {
        long maxLabel = _getMaxLabel(labelMap, xIndex, yIndex);
        if (labelMap[_getIndex(xIndex, yIndex)] != maxLabel) {
          labelMap[_getIndex(xIndex, yIndex)] = maxLabel;
          changes += 1;
        }
      }
      else {
        labelMap[_getIndex(xIndex, yIndex)] = -1;
      }
    }
  }
  return changes;
}

long Image::_getMaxLabel(long* labelMap, int xIndex, int yIndex) {
  int xMin = std::max(xIndex - 1, 0);
  int yMin = std::max(yIndex - 1, 0);
  int xMax = std::min(xIndex + 1, (int)(getWidth() - 1));
  int yMax = std::min(yIndex + 1, (int)(getHeight() - 1));

  long maxLabel = labelMap[_getIndex(xIndex, yIndex)];
  unsigned char pixelValue = getPixel(xIndex, yIndex);

  if(pixelValue == getPixel(xMin, yMin)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xMin, yMin)]);
  }
  if(pixelValue == getPixel(xMin, yIndex)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xMin, yIndex)]);
  }
  if(pixelValue == getPixel(xMin, yMax)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xMin, yMax)]);
  }
  if(pixelValue == getPixel(xIndex, yMin)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xIndex, yMin)]);
  }
  if(pixelValue == getPixel(xIndex, yMax)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xIndex, yMax)]);
  }
  if(pixelValue == getPixel(xMax, yMin)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xMax, yMin)]);
  }
  if(pixelValue == getPixel(xMax, yIndex)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xMax, yIndex)]);
  }
  if(pixelValue == getPixel(xMax, yMax)) {
    maxLabel = std::max(maxLabel, labelMap[_getIndex(xMax, yMax)]);
  }
  return maxLabel;
}

long Image::_getIndex(int xIndex, int yIndex) {
  return (yIndex * getWidth() + xIndex);
}

long Image::_countLabels(long* labelMap) {
  long count = 0;
  long countMap[getImageIndexCount()]{0};
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    if (countMap[labelMap[pixelIndex]] == 0 & getPixel(pixelIndex) == 255){
      count += 1;
    }
    countMap[labelMap[pixelIndex]] += 1;
  }
  return count;
}

// void Image::printRegionSizes() {
//   long* labelMap = getLabelMap(255);
//   std::set<int> regions = getUniqueLabels(labelMap);
//   Eigen::Vector3f lowerLeft{startingBox.xmin,startingBox.ymin, 1.0f};
//   std::map<int,
// }

std::map<int, bool> Image::testShapes(int tolerance){
  long* labelMap = getLabelMap(255);
  long countMap[getImageIndexCount()]{0};
  long pixelIndex;
  
  std::map<int, int> labelAreas = getLabelAreas(labelMap);

  // Flag labels in return object
  std::map<int, bool> shapeResults;
  for (auto const& labelArea : labelAreas) {
    if(labelArea.second > 0 & labelArea.second <= tolerance){
      shapeResults[labelArea.first] = true;
      std::cout<<"Good shape"<<std::endl;
    }
    else if(labelArea.first >= 0){  // exlude masked areas which are flagged -1
      shapeResults[labelArea.first] = false;
      std::cout<<"Bad shape: "<<labelArea.first<<std::endl;
    }
  }
  return shapeResults;
}

std::vector<std::tuple<float, float>> Image::getCenters() {
  long* labelMap = getLabelMap(255);
  std::set<int> uniqueLabels = getUniqueLabels(labelMap);

  // Initialize averaging maps
  std::map<int, std::tuple<long, long>> xTotals;
  std::map<int, std::tuple<long, long>> yTotals;
  for (auto const& uniqueLabel : uniqueLabels) {
    if(uniqueLabel >= 0){
      std::get<0>(xTotals[uniqueLabel]) = 0;
      std::get<1>(xTotals[uniqueLabel]) = 0;
      std::get<0>(yTotals[uniqueLabel]) = 0;
      std::get<1>(yTotals[uniqueLabel]) = 0;
    }
  }

  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    if(labelMap[pixelIndex] >= 0) {
      int yIndex = floor(pixelIndex / getWidth());
      int xIndex = pixelIndex - yIndex * getWidth();
      std::get<0>(xTotals[labelMap[pixelIndex]]) += xIndex;
      std::get<1>(xTotals[labelMap[pixelIndex]]) += 1;
      std::get<0>(yTotals[labelMap[pixelIndex]]) += yIndex;
      std::get<1>(yTotals[labelMap[pixelIndex]]) += 1;
    }
  }

  std::vector<std::tuple<float, float>> centers(uniqueLabels.size() - 1);
  int count = 0;
  for (auto const& uniqueLabel : uniqueLabels) {
    if(uniqueLabel >= 0) {
      float xAvg = (float)std::get<0>(xTotals[uniqueLabel]) / std::get<1>(xTotals[uniqueLabel]);
      float yAvg = (float)std::get<0>(yTotals[uniqueLabel]) / std::get<1>(yTotals[uniqueLabel]);
      std::get<0>(centers[count]) = xAvg;
      std::get<1>(centers[count]) = yAvg;
      std::cout<<"Center found: "<<xAvg<<", "<<yAvg<<std::endl;
      count += 1;
    }
  }
  return centers;
}

std::map<int, int> Image::getLabelAreas(){
  long* labelMap = getLabelMap(255);
  return getLabelAreas(labelMap);
}

std::map<int, int> Image::getLabelAreas(long* labelMap){
  std::set<int> uniqueLabels = getUniqueLabels(labelMap);
  std::map<int, int> labelAreas;
  // Initialize counters
  for (auto const& uniqueLabel : uniqueLabels) {
    labelAreas[uniqueLabel] = 0;
  }

  // Sum areas
  int pixelIndex = 0;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++) {
    labelAreas[labelMap[pixelIndex]] += 1;
  }
  return labelAreas;
}
