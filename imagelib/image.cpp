#include "image.hpp"

#include <eigen3/Eigen/Core> // access to vectors and matrices
#include <iostream>
#include <math.h>

Image::Image(){};

Image::Image(std::string filename){
  openFile(filename);
}

Image::Image(std::string filename1, std::string filename2, std::string filename3){
  combineFiles(filename1, filename2, filename3);
}

Image::Image(long width, long height, long channels, long bpc, long depth){
  setWidth(width);
  _height = height;
  _channels = channels;
  _bpc = bpc;
  _depth = depth;
  createCanvas();
}

Image::Image(Image* originalImage){
  _height = originalImage->getHeight();
  _width = originalImage->getWidth();
  _channels = originalImage->getChannels();
  _bpc = originalImage->getBPC();
  _depth = originalImage->getDepth();
  createCanvas();
  memcpy(_data, originalImage->getImageData(), getImageSize());
}

Image::~Image() {
  delete[] _data;
};

bool Image::openFile(std::string filename){
  // Check file type by filename extensions
  if(isTiffFile(filename)){
    return loadTiff(filename);
  }
  else if(isJpegFile(filename)){
    return loadJpeg(filename);
  }
  else {
    std::cout<<"Unknown extension..."<<std::endl;
  };
  return false;
};

bool Image::replaceImage(Image newImage){
  std::cout<<"Setting width..."<<std::endl;
  _width = newImage.getWidth();
  _height = newImage.getHeight();
  _channels = newImage.getChannels();
  createCanvas();
  // memcpy(_data,newImage.getImageData(),_width * _height * _channels);
  std::cout<<"Done cloning image elemnts..."<<std::endl;
  return true;
}

bool Image::combineFiles(std::string filename1, std::string filename2, std::string filename3) {
  Image image_1(filename1);
  Image image_2(filename2);
  Image image_3(filename3);

  // TODO: add checks that all images are the same dimensions and depth
  // All channels must be 1
  _width = image_1.getWidth();
  _height = image_1.getHeight();
  _depth = image_1.getDepth();
  _channels = 3;
  
  std::cout<<"Combining files..."<<std::endl;
  long data_size = image_1.getWidth() * image_1.getHeight() * 3;
  long old_data_size = data_size / 3;
  long source_data_index;
  unsigned char* new_data = (unsigned char*)malloc(data_size);
  std::cout<<"New image size: "<<data_size<<std::endl;
  for (source_data_index=0; source_data_index < old_data_size; source_data_index++) {
    int new_data_index = source_data_index * 3;
    if (source_data_index >= old_data_size || new_data_index >= data_size) {
      std::cout<<"New index: "<<new_data_index<<std::endl;
      std::cout<<"Source index: "<<source_data_index<<std::endl;
    }
    new_data[new_data_index] = *(image_1.getImageData() + source_data_index);
    new_data[new_data_index + 1] = *(image_2.getImageData() + source_data_index);
    new_data[new_data_index + 2] = *(image_3.getImageData() + source_data_index);
  };
  std::cout<<"Files combined..."<<std::endl;
  _data = new_data;

  int str_length = filename1.length();
  std::string new_filename = filename1.substr(0, str_length-3) +  "combined.tif";
  saveTiff(new_filename);

  return true;
};

unsigned char* Image::getImageData(){
     return _data;
};

unsigned long Image::getImageSize(){
  return _width * _height * _channels * _bpc / 8;
}

unsigned long Image::getImageIndexCount(){
  return _width * _height * _channels;
}

bool Image::isTiffFile(std::string filename){
  std::string ext = Image::getExtension(filename);
  if(ext == ".tif" || ext == ".svs" || ext == ".tiff"){
    return true;
  };
  return false;
};

bool Image::isJpegFile(std::string filename){
  std::string ext = Image::getExtension(filename);
  if(ext == ".jpg"){
    return true;
  };
  return false;
};

std::string Image::getExtension(std::string filename){
  int str_length = filename.length();
  int fileExtPos = filename.find_last_of('.');
  if (fileExtPos == std::string::npos) {
    std::cout<<"Error in filename, no filetype found: "<<filename<<std::endl;
    return "";
  }
  return filename.substr(fileExtPos);
};

bool Image::createCanvas(){
  _data = (unsigned char*)malloc(getImageSize());
  return true;
}

bool Image::setWidth(unsigned long newWidth){
  _width = newWidth;
  return true;
}

bool Image::setHeight(unsigned long newHeight){
  _height = newHeight;
  return true;
}

bool Image::setPixel(long pixelIndex, unsigned char pixelValue){
  if(pixelIndex < 0 || pixelIndex >= _width * _height * _channels) {
    std::cout<<"Set pixel out of range: "<<pixelIndex<<std::endl;
  }
  _data[pixelIndex] = pixelValue;
  return true;
}

unsigned char Image::getPixel(long pixelIndex){
  if(pixelIndex < 0 || pixelIndex >= _width * _height * _channels) {
    std::cout<<"Get pixel out of range: "<<pixelIndex<<std::endl;
    return _backgroundColor;
  }
  return (unsigned char)_data[pixelIndex];
}

unsigned char Image::getPixel(Eigen::Vector3f &idx, long channel, bool useBiLinear){
  if (idx[0] < 0 || idx[0] >= _width || idx[1] < 0 || idx[1] >= _height) {
    unsigned char backgroundColor{0};
    return backgroundColor;
  }
  if (useBiLinear) {
    return getPixelBiLinear(idx, channel);
  }
  return getNearestPixel(idx, channel);
}

unsigned char Image::getPixel(int x, int y, long channel){
  if (x < 0 || x >= _width || y < 0 || y >= _height) {
    return _backgroundColor;
  }
  long imageIndex = (y * (_width) + x) * _channels + channel;
  if(imageIndex < 0 || imageIndex >= getImageIndexCount()) {
    return _backgroundColor;
  }
  return getPixel(imageIndex);
}

unsigned char Image::getPixel(int x, int y){
  return getPixel(x, y, 0);
}

bool Image::setPixel(int x, int y, long channel, unsigned char pixelValue){
  if (x < 0 || x >= _width || y < 0 || y >= _height) {
    return false;
  }
  long imageIndex = (y * (_width) + x) * _channels + channel;
  return setPixel(imageIndex, pixelValue);
}

bool Image::setPixelwChannel(long pixelIndex, long channel, unsigned char pixelValue){
  if (pixelIndex < 0 || pixelIndex >= getImageIndexCount()) {
    return false;
  }
  long imageIndex = pixelIndex * _channels + channel;
  return setPixel(imageIndex, pixelValue);
}

unsigned char Image::getNearestPixel(Eigen::Vector3f &idx, long channel){
  long xIndex = round(idx[0]);
  long yIndex = round(idx[1]);
  long imageIndex = (yIndex * (_width) + xIndex) * _channels + channel;
  if(imageIndex < 0 || imageIndex >= _width * _height * _channels - 1) {
    unsigned char backgroundColor2{0};
    return backgroundColor2;
  }
  return getPixel(imageIndex);
}

unsigned char Image::getPixelBiLinear(Eigen::Vector3f &idx, long channel){
  float xLocation = idx[0] - floor(idx[0]);
  float yLocation = idx[1] - floor(idx[1]);

  Eigen::Vector3f neighborIndexLowerLeft = {float(floor(idx[0])), float(floor(idx[1])), 1.0f};
  Eigen::Vector3f neighborIndexLowerRight = {float(ceil(idx[0])), float(floor(idx[1])), 1.0f};
  Eigen::Vector3f neighborIndexUpperLeft = {float(floor(idx[0])), float(ceil(idx[1])), 1.0f};
  Eigen::Vector3f neighborIndexUpperRight = {float(ceil(idx[0])), float(ceil(idx[1])), 1.0f};

  unsigned char neighborValueLowerLeft = getPixel(neighborIndexLowerLeft, channel);
  unsigned char neighborValueLowerRight = getPixel(neighborIndexLowerRight, channel);
  unsigned char neighborValueUpperLeft = getPixel(neighborIndexUpperLeft, channel);
  unsigned char neighborValueUpperRight = getPixel(neighborIndexUpperRight, channel);

  unsigned char biLinearValue = (
    (1 - xLocation) * (1 - yLocation) * neighborValueLowerLeft +
    xLocation * (1 - yLocation) * neighborValueLowerRight +
    (1 - xLocation) * yLocation * neighborValueUpperLeft +
    xLocation * yLocation * neighborValueUpperRight);
  return biLinearValue;
}

bool Image::saveOutput(std::string filename) {
  saveTiff(filename);
  return true;
}

void Image::makeThreeChannel() {
  unsigned char tempData[getImageIndexCount()]{0};
  memcpy(tempData, _data, getImageIndexCount());
  long singleChannelIndexCount = getImageIndexCount();
  setChannels(3);
  createCanvas();
  long pixelIndex;
  for(pixelIndex = 0; pixelIndex < singleChannelIndexCount; pixelIndex++) {
    setPixelwChannel(pixelIndex, 0, tempData[pixelIndex]);
    setPixelwChannel(pixelIndex, 1, tempData[pixelIndex]);
    setPixelwChannel(pixelIndex, 2, tempData[pixelIndex]);
  }
}

unsigned long Image::getWidth() {return _width;};
unsigned long Image::getHeight() { return _height;};
unsigned long Image::getDepth() { return _depth;} ;
unsigned long Image::getChannels() { return _channels; };
void Image::setChannels(long channels) { _channels = channels; };
unsigned long Image::getBPC() { return _bpc;};

