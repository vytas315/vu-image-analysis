#include <iostream>
#include <math.h>

#include "image.hpp"

bool ImageFloat::applyLaplacian(Image* originalImage){
  int x, y;

  for(x = 0; x < getWidth(); x++) {
    for(y = 0; y < getHeight(); y++) {
      int leftIndex = std::max(x - 1, 0);
      int rightIndex = std::min(x + 1, (int)getWidth() - 1);
      int aboveIndex = std::max(y - 1, 0);
      int belowIndex = std::min(y + 1, (int)getHeight() - 1);
      float results = 0;
      results = originalImage->getPixel(leftIndex, y, 0) +
        originalImage->getPixel(rightIndex, y, 0) +
        originalImage->getPixel(x, aboveIndex, 0) +
        originalImage->getPixel(x, belowIndex, 0) +
        originalImage->getPixel(rightIndex, aboveIndex, 0) +
        originalImage->getPixel(leftIndex, aboveIndex, 0) +
        originalImage->getPixel(rightIndex, belowIndex, 0) +
        originalImage->getPixel(leftIndex, belowIndex, 0) -
        8 * originalImage->getPixel(x, y, 0);
      
      setPixel(x,y,0,-1.0f * results);
    }
  }
  return true;
}

bool ImageFloat::applySobel(Image* originalImage){
  int x, y;
  for(x = 0; x < getWidth(); x++) {
    for(y = 0; y < getHeight(); y++) {
      int leftIndex = std::max(x - 1, 0);
      int rightIndex = std::min(x + 1, (int)getWidth() - 1);
      int aboveIndex = std::max(y - 1, 0);
      int belowIndex = std::min(y + 1, (int)getHeight() - 1);
      float gx = 0;
      float gy = 0;
      gx = std::abs(-1.0f * originalImage->getPixel(leftIndex, aboveIndex, 0) +
        originalImage->getPixel(rightIndex, aboveIndex, 0) +
        -2.0f * originalImage->getPixel(leftIndex, y, 0) +
        2.0f * originalImage->getPixel(rightIndex, y, 0) +
        -1.0f * originalImage->getPixel(leftIndex, belowIndex, 0) +
        originalImage->getPixel(rightIndex, belowIndex, 0));
      gy = std::abs(-1.0f * originalImage->getPixel(leftIndex, aboveIndex, 0) +
        -2.0f * originalImage->getPixel(x, aboveIndex, 0) +
        -1.0f * originalImage->getPixel(rightIndex, aboveIndex, 0) +
        originalImage->getPixel(leftIndex, belowIndex, 0) +
        2.0f * originalImage->getPixel(x, belowIndex, 0) +
        originalImage->getPixel(rightIndex, belowIndex, 0));
      
      setPixel(x, y, 0, sqrt(pow(gx, 2) + pow(gy, 2)));
    }
  }
  return true;
}

bool ImageFloat::apply3x3Smoothing(Image* originalImage){
  int x, y;
  for(x = 0; x < getWidth(); x++) {
    for(y = 0; y < getHeight(); y++) {
      int leftIndex = std::max(x - 1, 0);
      int rightIndex = std::min(x + 1, (int)getWidth() - 1);
      int aboveIndex = std::max(y - 1, 0);
      int belowIndex = std::min(y + 1, (int)getHeight() - 1);
      float results = (originalImage->getPixel(leftIndex, aboveIndex, 0) +
        originalImage->getPixel(x, aboveIndex, 0) +
        originalImage->getPixel(rightIndex, aboveIndex, 0) +
        originalImage->getPixel(leftIndex, y, 0) +
        originalImage->getPixel(x, y, 0) +
        originalImage->getPixel(rightIndex, y, 0) +
        originalImage->getPixel(leftIndex, belowIndex, 0) +
        originalImage->getPixel(x, belowIndex, 0) +
        originalImage->getPixel(rightIndex, belowIndex, 0))/ 9.0f;
      setPixel(x, y, 0, results);
    }
  }
  return true;
}

bool ImageFloat::add(Image* originalImage){
  int pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    setPixel(pixelIndex, getPixel(pixelIndex) + originalImage->getPixel(pixelIndex));
  }
  return true;
}

bool ImageFloat::multiply(Image* originalImage){
  int pixelIndex;
  for(pixelIndex = 0; pixelIndex < getImageIndexCount(); pixelIndex++){
    setPixel(pixelIndex, getPixel(pixelIndex) * originalImage->getPixel(pixelIndex));
  }
  return true;
}

void Image::adaptiveMedianNoiseReduction(int sMin, int sMax){
  unsigned char newImageData[getImageIndexCount()]{0};
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      newImageData[_getIndex(xIndex, yIndex)] = _getAdaptiveNoiseValue(xIndex, yIndex, sMin, sMax);
    }
  }
  memcpy(_data, newImageData, getImageIndexCount());
}

unsigned char Image::_getAdaptiveNoiseValue(int xIndex, int yIndex, int sSize, int sMax) {
  int seXindex, seYindex;
  int values[sSize * sSize];
  int count = 0;
  for(seXindex = xIndex - (sSize - 1) / 2; seXindex <= xIndex + (sSize - 1) / 2; seXindex++) {
    for(seYindex = yIndex - (sSize - 1) / 2; seYindex <= yIndex + (sSize - 1) / 2; seYindex++) {
      if(seXindex >= 0 & seYindex >= 0 & seXindex < getWidth() & seYindex < getHeight()) {
        if(seXindex != xIndex | seYindex != yIndex) {
          values[count] = (int)getPixel(seXindex, seYindex);
          count++;
        }
      }
    }
  }
  int* minValue = std::min_element(values, values + count);
  // std::cout<<"minValue: "<<*minValue<<std::endl;
  int* maxValue = std::max_element(values, values + count);
  // std::cout<<"maxValue: "<<*maxValue<<std::endl;
  int medValue = _median(values, count);
  // std::cout<<"medValue: "<<medValue<<std::endl;
  int value = (int)getPixel(xIndex, yIndex);
  // std::cout<<"value: "<<value<<std::endl;

  int aOne = (medValue - (int)(*minValue));
  int aTwo = medValue - (int)(*maxValue);
  // std::cout<<"A1: "<<aOne<<std::endl;
  // std::cout<<"A2: "<<aTwo<<std::endl;
  if(aOne > 0 and aTwo < 0) {
    // Stage B
    int bOne = value - *minValue;
    int bTwo = value - *maxValue;
    if (bOne > 0 & bTwo < 0) {
      return (unsigned char)value;
    }
    else {
      std::cout<<"Pixel changed"<<std::endl;
      return (unsigned char)medValue;
    }
  }
  else {
    if(sSize >= sMax) {
      return (unsigned char)value;
    }
    else {
      return _getAdaptiveNoiseValue(xIndex, yIndex, sSize + 2, sMax);
    }
  }
}

void Image::medianFilter(int size){
  unsigned char newImageData[getImageIndexCount()]{0};
  int xIndex, yIndex;
  for(xIndex = 0; xIndex < getWidth(); xIndex++) {
    for(yIndex = 0; yIndex < getHeight(); yIndex++) {
      newImageData[_getIndex(xIndex, yIndex)] = _getMedian(xIndex, yIndex, size);
    }
  }
  memcpy(_data, newImageData, getImageIndexCount());
}

unsigned char Image::_getMedian(int xIndex, int yIndex, int size) {
  int seXindex, seYindex;
  int values[size * size];
  int count = 0;
  for(seXindex = xIndex - (size - 1) / 2; seXindex <= xIndex + (size - 1) / 2; seXindex++) {
    for(seYindex = yIndex - (size - 1) / 2; seYindex <= yIndex + (size - 1) / 2; seYindex++) {
      if(seXindex >= 0 & seYindex >= 0 & seXindex < getWidth() & seYindex < getHeight()) {
        // if(seXindex != xIndex | seYindex != yIndex) {
          values[count] = (int)getPixel(seXindex, seYindex);
          count++;
        // }
      }
    }
  }
  return _median(values, count);
}

unsigned char Image::_median(int a[], int n) {
    std::sort(a, a + n);
    return a[(int)floor(n / 2)];
}
