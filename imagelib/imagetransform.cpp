#include<cstring>
#include <eigen3/Eigen/Core> // access to vectors and matrices
#include <eigen3/Eigen/LU> // access to factorization methods , particalarly inverse−function .
#include <iostream>
#include <math.h>

#include "image.hpp"

// World coordinates are of the new image
// Indices are coordinates of the old image

bool Image::transformImage() {
  std::cout<<"Transforming image..."<<std::endl;
  
  Eigen::Matrix3f affine_transform; // get matrix
  affine_transform.setIdentity(); // clear to identity
  affine_transform(0,1) = 0.1f;  // Shear X
  affine_transform(1,0) = 0.1f;  // Shear Y
  affine_transform(0,2) = -100.0f;  // Translate X
  affine_transform(1,2) = -100.0f;  // Translate Y
  affine_transform(0,0) = 1.0f;  // Scale X
  affine_transform(1,1) = 1.0f;  // Scale Y

  std::cout<<affine_transform<<std::endl;

  Eigen::Matrix3f translate;
  translate.setIdentity();
  translate(1,2) = -1.0f*(_height-1.0f);  // Translate Y

  Eigen::Matrix3f flip_coordinates;
  flip_coordinates.setIdentity();
  flip_coordinates(1,1) = -1.0f;  // Flip Y

  Image::boundingBox transformedBox = getTransformedBoundingBox(affine_transform);
  Image newImage = getNewImage(transformedBox);

  Eigen::Matrix3f translate_prime;
  translate_prime.setIdentity();
  // Shift given the new height of the image and any negative Y region of the bounding box
  translate_prime(1,2) = -1.0f * (newImage.getHeight() + std::min(transformedBox.ymin, 0.0f));
  // Shift any negative X region in the bounding box.
  translate_prime(0,2) = std::min(transformedBox.xmin, 0.0f);

  Eigen::Matrix3f I_w =  flip_coordinates * translate;
  Eigen::Matrix3f W_i = I_w.inverse();

  Eigen::Matrix3f I_w_prime =  flip_coordinates * translate_prime;
  Eigen::Matrix3f W_i_prime = I_w_prime.inverse();
  
  
  unsigned long newXIndex, newYIndex, currentChannel;
  unsigned long currentIndex = 0;
  std::cout<<"Loading new pixels..."<<std::endl;
  for(newYIndex = 0; newYIndex < newImage.getHeight(); newYIndex++) {
    for(newXIndex = 0; newXIndex < newImage.getWidth(); newXIndex++) {
      Eigen::Vector3f index_prime{float(newXIndex),float(newYIndex),1.0};
      Eigen::Vector3f index_original = W_i * affine_transform.inverse() * I_w_prime * index_prime;
      if(round(index_original[0]) == 1599 && round(index_original[1]) == 633){
        std::cout<<"Index prime: "<<index_prime<<std::endl;
        std::cout<<"Index original: "<<index_original<<std::endl;
      }
      for(currentChannel = 0; currentChannel < newImage.getChannels(); currentChannel++) {
        unsigned long newImageIndex = (newYIndex * (newImage.getWidth()) + newXIndex) * 
          newImage.getChannels() + currentChannel;        
        unsigned char newPixelValue = getPixel(index_original, currentChannel, true);
        if(newImageIndex > newImage.getWidth() * newImage.getHeight() * newImage.getChannels() - 1) {
          std::cout<<"Index out of range..."<<std::endl;
        }
        newImage.setPixel(currentIndex, newPixelValue);
        currentIndex++;
      }
    }
  }
  _width = newImage.getWidth();
  _height = newImage.getHeight();
  _channels = newImage.getChannels();
  _bpc = newImage.getBPC();
  createCanvas();
  memcpy(_data,newImage.getImageData(),_width * _height * _channels);
  return true;
}

Image Image::getNewImage(Image::boundingBox transformedBox) {
  std::cout<<"Width float: "<<float(_width)<<std::endl;
  std::cout<<"X min: "<<transformedBox.xmin<<std::endl;
  unsigned long newWidth = std::max(ceil(transformedBox.xmax), float(_width))-std::min(floor(transformedBox.xmin),0.0f) + 1;
  unsigned long newHeight = std::max(ceil(transformedBox.ymax), float(_height))-std::min(floor(transformedBox.ymin), 0.0f) + 1;
  unsigned long newImageSize = newWidth * newHeight * _channels;
  std::cout<<"New width: "<<newWidth<<std::endl;
  std::cout<<"New height: "<<newHeight<<std::endl;
  std::cout<<"New image size: "<<newImageSize<<std::endl;
  Image new_image(newWidth, newHeight, _channels, _bpc, _depth);
  return new_image;
}

Image::boundingBox Image::getTransformedBoundingBox(Eigen::Matrix3f transformMatrix) {
  boundingBox startingBox, transformedBox;

  startingBox.xmin = 0;
  startingBox.ymin = 0;
  startingBox.xmax = _width - 1;
  startingBox.ymax = _height - 1;

  Eigen::Vector3f lowerLeft{startingBox.xmin,startingBox.ymin, 1.0f};
  Eigen::Vector3f upperLeft{startingBox.xmin,startingBox.ymax, 1.0f};
  Eigen::Vector3f lowerRight{startingBox.xmax,startingBox.ymin, 1.0f};
  Eigen::Vector3f upperRight{startingBox.xmax,startingBox.ymax, 1.0f};

  Eigen::Vector3f transformedLowerLeft = transformMatrix * lowerLeft;
  Eigen::Vector3f transformedUpperLeft = transformMatrix * upperLeft;
  Eigen::Vector3f transformedLowerRight = transformMatrix * lowerRight;
  Eigen::Vector3f transformedUpperRight = transformMatrix * upperRight;

  transformedBox.xmin = std::min({transformedLowerLeft[0], transformedUpperLeft[0], transformedLowerRight[0], transformedUpperRight[0]});
  transformedBox.xmax = std::max({transformedLowerLeft[0], transformedUpperLeft[0], transformedLowerRight[0], transformedUpperRight[0]});
  transformedBox.ymin = std::min({transformedLowerLeft[1], transformedUpperLeft[1], transformedLowerRight[1], transformedUpperRight[1]});
  transformedBox.ymax = std::max({transformedLowerLeft[1], transformedUpperLeft[1], transformedLowerRight[1], transformedUpperRight[1]});
  
  return transformedBox;
}
